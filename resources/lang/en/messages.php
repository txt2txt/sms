<?php

return [

	'auth' => [
		'failed' => 'Invalid login credentials.'
	],

	'ctr-notifications' => [
		'request-login-url' => 'If your email exists in our system, a link was sent to your email address!',
		'account-unlocked' => 'Your account was successfully unlocked! You may now login.',
		'reset-login-url' => 'Your login url was successfully reset! You may now login.',
		'password-reset-request' => 'If your email was found, a link was sent to your email address!',
		'password-reset-success' => 'Your password was reset, you may now login!',
	],

	'token' => [
		'activation' => 'Activation token is invalid or expired.',
		'account-unlock' => 'The token is invalid or expired.',
		'invalid-url' => 'Url regeneration token is invalid or expired.',
		'password-reset' => 'Your password reset token is invalid or expired.',
	]

];