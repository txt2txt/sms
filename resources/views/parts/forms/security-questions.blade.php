<div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">

        <div class="modal-header primary-color white-text light-blue darken-4">
            <h4 class="title">
                <i class="fa fa-lock"></i>
                Security Questions
            </h4>
        </div>

        <div class="modal-body">

            <form method="post">

                <div class="md-form">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="password" id="password" class="form-control" name="password" placeholder="New password...">
                </div>
                @if($errors->has('password'))
                    <p class="alert alert-danger text-center">{{ $errors->first('password') }}</p>
                @endif

                <div class="md-form">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm new password...">
                </div>
                @if($errors->has('cpassword'))
                    <p class="alert alert-danger text-center">{{ $errors->first('cpassword') }}</p>
                @endif

                <div class="text-center mt-4">
                    <button class="btn btn btn-primary light-blue darken-4" type="submit">Submit</button>
                </div>

                {{ csrf_field() }}
            </form>

        </div>

        <div class="modal-footer white-text light-blue darken-4">

        </div>

    </div>

</div>