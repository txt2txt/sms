<div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">

        <div class="modal-header primary-color white-text light-blue darken-4">
            <h4 class="title">
                <i class="fa fa-question-circle-o"></i>
                Reset your Password by....
            </h4>
        </div>

        <div class="modal-body">

            <div class="text-center mt-4 mb-2">

                <a href="{{ routeFor('password-reset-by-email') }}" class="btn btn-primary light-blue darken-4">Emailed link!</a>

                <a href="{{ routeFor('password-reset-by-questions') }}" class="btn btn-primary light-blue darken-4">Security Questions!</a>
            </div>

        </div>

        <div class="modal-footer white-text light-blue darken-4">
            <a href="{{ routeFor('login') }}" class="text-white">Back to Login?</a>
        </div>

    </div>

</div>