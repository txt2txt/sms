<div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">

        <div class="modal-header primary-color white-text light-blue darken-4">
            <h4 class="title">
                <i class="fa fa-lock"></i>
                Login
            </h4>
        </div>

        <div class="modal-body">

            <form method="post">

                @if(\Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                @if($errors->has('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ $errors->first('error') }}</p>
                        <p>You have {{ Session::get(\SMS\Enums\Settings::$FailedLoginAttemptsRemaining) }} login attempts remaining before your account is locked.</p>
                    </div>
                @endif

                <div class="md-form">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your email...">
                </div>
                @if($errors->has('email'))
                    <p class="alert alert-danger text-center">{{ $errors->first('email') }}</p>
                @endif

                <div class="md-form">
                    <i class="fa fa-lock prefix grey-text"></i>
                    <input type="password" id="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Your password...">
                </div>
                @if($errors->has('password'))
                    <p class="alert alert-danger text-center">{{ $errors->first('password') }}</p>
                @endif

                <div class="text-center mt-4">
                    <button class="btn btn btn-primary light-blue darken-4 waves-effect waves-light" type="submit">Login</button>
                </div>

                {{ csrf_field() }}
            </form>

        </div>

        <div class="modal-footer white-text light-blue darken-4">
            <a href="{{ routeFor('password-reset-by-email') }}" class="text-white">Forgot Password?</a>
        </div>

    </div>

</div>