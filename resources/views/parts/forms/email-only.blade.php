<div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">

        <div class="modal-header primary-color white-text light-blue darken-4">
            <h4 class="title">
                <i class="fa fa-lock"></i>
                @if(!empty($formTitle))
                    {{ $formTitle }}
                @endif
            </h4>
        </div>

        <div class="modal-body">

            <form method="post">

                @if(\Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                @if(\Session::has('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ $errors->first('email') }}
                    </div>
                @endif

                <div class="md-form">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your email...">
                </div>

                <div class="text-center mt-4">
                    <button class="btn btn btn-primary light-blue darken-4" type="submit">Submit</button>
                </div>

                {{ csrf_field() }}
            </form>

        </div>

        <div class="modal-footer white-text light-blue darken-4">
            @if(!empty($linkText) && !empty($linkUrl))
                <a href="{{ $linkUrl }}" class="text-white">{{ $linkText }}</a>
            @endif
        </div>

    </div>

</div>