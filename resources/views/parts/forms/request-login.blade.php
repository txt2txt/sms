<div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">

        <div class="modal-header primary-color white-text">
            <h4 class="title">
                <i class="fa fa-lock"></i> Request Login Link
            </h4>
        </div>

        <div class="modal-body">

            @if(\Session::has('success'))
                <div class="alert alert-primary" role="alert">
                    An email was sent to your email address with your special login url (<small><em>If your email address was found on file</em></small>). <br> Please bookmark your login url for logging into this site in the future.
                </div>
            @endif

            <form method="post">

                <div class="md-form form-sm">
                    <i class="fa fa-envelope prefix"></i>
                    <input type="email" id="email" name="email" class="form-control form-control-sm">
                    <label for="email">Your Email</label>
                </div>

                <div class="text-center mt-4 mb-2">
                    <button class="btn btn btn-outline-indigo waves-effect waves-light">Request Link
                        <i class="fa fa-send ml-2"></i>
                    </button>
                </div>

                {{ csrf_field() }}

            </form>

        </div>

    </div>

</div>
