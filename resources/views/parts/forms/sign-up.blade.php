<div class="modal-dialog cascading-modal" role="document">

    <div class="modal-content">

        <div class="modal-header {{ $errors->Count() > 0 ? 'danger-color' : 'primary-color light-blue darken-4' }} white-text">
            <h4 class="title">
                <i class="fa fa-user"></i> Sign Up
            </h4>
        </div>

        <div class="modal-body">

            @if(\Session::has('success'))
                <div class="alert alert-primary" role="alert">
                    <p>An activation link has been sent to your email address. Please click on the link to activate your account.</p>
                </div>
            @endif

            @if(!\Session::has('success'))

                <form method="post">

                    <div class="md-form form-sm">
                        <i class="fa fa-user prefix"></i>
                        <input type="text" id="fname" name="fname" class="form-control form-control-sm" value="{{ old('fname') }}">
                        <label for="fname">Your First Name</label>
                    </div>
                    @if($errors->has('fname'))
                        <p class="alert alert-danger text-center">{{ $errors->first('fname') }}</p>
                    @endif


                    <div class="md-form form-sm">
                        <i class="fa fa-user prefix"></i>
                        <input type="text" id="lname" name="lname" class="form-control form-control-sm" value="{{ old('lname') }}">
                        <label for="lname">Your Last Name</label>
                    </div>
                    @if($errors->has('lname'))
                        <p class="alert alert-danger text-center">{{ $errors->first('lname') }}</p>
                    @endif


                    <div class="md-form form-sm">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="email" id="email" name="email" class="form-control form-control-sm" value="{{ old('email') }}">
                        <label for="email">Your Email</label>
                    </div>
                    @if($errors->has('email'))
                        <p class="alert alert-danger text-center">{{ $errors->first('email') }}</p>
                    @endif


                    <div class="md-form form-sm">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="email" id="cemail" name="cemail" class="form-control form-control-sm" value="{{ old('cemail') }}">
                        <label for="cemail">Confirm Your Email</label>
                    </div>
                    @if($errors->has('cemail'))
                        <p class="alert alert-danger text-center">{{ $errors->first('cemail') }}</p>
                    @endif


                    <div class="md-form form-sm">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" id="password" name="password" class="form-control form-control-sm">
                        <label for="password">Your Password</label>
                    </div>
                    @if($errors->has('password'))
                        <p class="alert alert-danger text-center">{{ $errors->first('password') }}</p>
                    @endif


                    <div class="md-form form-sm">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" id="cpassword" name="cpassword" class="form-control form-control-sm">
                        <label for="cpassword">Confirm Your Password</label>
                    </div>
                    @if($errors->has('cpassword'))
                        <p class="alert alert-danger text-center">{{ $errors->first('cpassword') }}</p>
                    @endif

                    <div class="text-center mt-4 mb-2">
                        <button class="bbtn btn btn-primary light-blue darken-4 waves-effect waves-light">Sign Up
                            <i class="fa fa-send ml-2"></i>
                        </button>
                    </div>

                    {{ csrf_field() }}

                </form>

            @endif

        </div>

    </div>

</div>