<div class="list-group list-group-flush">

    <a href="{{ routeFor(\SMS\Enums\RouteNames::$ProfileHome) }}" class="list-group-item list-group-item-action waves-effect  @if(pageName(\SMS\Enums\RouteNames::$ProfileHome)) active @endif">
        <i class="fa fa-user"></i> Profile
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-gears"></i> Preferences
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-address-card-o"></i> Contact Info
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-user-circle-o"></i> Personal Info
    </a>

    <a href="{{ routeFor('profile-password-reset') }}" class="list-group-item list-group-item-action waves-effect @if(pageName(\SMS\Enums\RouteNames::$ProfilePasswordReset)) active @endif">
        <i class="fa fa-lock"></i> Reset Password
    </a>

    <a href="{{ routeFor('profile-activity') }}" class="list-group-item list-group-item-action waves-effect @if(pageName(\SMS\Enums\RouteNames::$ProfileActivity)) active @endif">
        <i class="fa fa-flash"></i> Activity
    </a>

    <a href="{{ routeFor(\SMS\Enums\RouteNames::$Logout)  }}" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-sign-out"></i> Logout
    </a>
</div>
