<div class="list-group list-group-flush">

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-address-book"></i> Recipients
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-users"></i> Recipient Groups
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-mobile"></i> Messages
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-commenting"></i> Notifications
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-gear"></i> Settings
    </a>

    <a href="#" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-link"></i> API
    </a>

    <a href="{{ url()->route('logout', Request::route('userId') ) }}" class="list-group-item list-group-item-action waves-effect">
        <i class="fa fa-sign-out"></i> Logout
    </a>

</div>
