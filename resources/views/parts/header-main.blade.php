<nav class="navbar navbar-expand-lg navbar-dark light-blue darken-4">

    <a class="navbar-brand" href="{{ url('/') }}">
        Txt2Txt
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="mainNav" style="">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link waves-effect waves-light" href="{{ url('/') }}">
                    <i class="fa fa-home"></i> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light" href="{{ url('/features') }}">
                    <i class="fa fa-gears"></i> Features
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light" href="{{ url('/pricing') }}">
                    <i class="fa fa-dollar"></i> Pricing
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light" href="{{ url('/about') }}">
                    <i class="fa fa-industry"></i> About
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light" href="{{ url('/connect') }}">
                    <i class="fa fa-plug"></i> Connect
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto nav-flex-icons">
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">
                    <i class="fa fa-linkedin"></i>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>

            <li class="nav-item dropdown">

                @auth
                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="userNavDropdown" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="userNavDropdown">
                        <a class="dropdown-item waves-effect waves-light" href="{{ routeFor('dash-home') }}">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                        <a class="dropdown-item waves-effect waves-light" href="#">
                            <i class="fa fa-user"></i> Profile
                        </a>
                        <a class="dropdown-item waves-effect waves-light" href="{{ routeFor('logout') }}">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>
                    </div>
                @endauth

                @guest
                        <a class="nav-link dropdown-toggle waves-effect waves-light" id="userNavDropdown" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="userNavDropdown">
                            <a class="nav-link waves-effect waves-light" href="{{ url('/request-login') }}">
                                <i class="fa fa-lock"></i> Login
                            </a>
                            <a class="nav-link waves-effect waves-light" href="{{ url('/sign-up') }}">
                                <i class="fa fa-user-circle-o"></i> Sign Up
                            </a>
                        </div>
                @endguest


            </li>
        </ul>
    </div>
</nav>

