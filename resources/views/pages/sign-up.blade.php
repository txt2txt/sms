@extends('layouts.main-rsb')

@section('page')

    <div class="container" id="app">

        @include('parts.forms.sign-up')

    </div>

@endsection