@extends('layouts.main-rsb')

@section('page')

    <div class="container" id="app">

        @include('parts.forms.email-only', ['formTitle' => 'Request Login  Link'] )

    </div>

@endsection