@extends('layouts.main-center')

@section('page-title', 'Login')

@section('page')

    @include('parts.forms.email-only', ['formTitle' => 'Request Password Reset Email', 'linkText' => 'Back to Login?', 'linkUrl' => routeFor('login')] )

@endsection