@extends('layouts.dash')

@section('page-title', 'Dashboard Home')

@section('nav')
    @include('parts.navs.dashboard')
@endsection

@section('page')

    <div class="container">

        <div class="row">

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#" class="text-blue-grey lighten-5">
                            <i class="fa fa-address-book"></i><br/>Recipients
                        </a>
                    </p>
                </div>
            </div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-users"></i><br/>Groups
                        </a>
                    </p>
                </div>
            </div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-mobile"></i><br/>Messages
                        </a>
                    </p>
                </div>
            </div>

            <div class="w-100 home-panel-icon-divider"></div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-commenting"></i><br/>Notifications
                        </a>
                    </p>
                </div>
            </div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-gear"></i><br/>Settings
                        </a>
                    </p>
                </div>
            </div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-link"></i><br/>API
                        </a>
                    </p>
                </div>
            </div>

        </div>

    </div>

@endsection

