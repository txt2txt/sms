@extends('layouts.dash')

@section('page-title', 'Profile')

@section('nav')
    @include('parts.navs.profile')
@endsection

@section('page')

    @include('parts.forms.password-reset')

@endsection