@extends('layouts.dash')

@section('page-title', 'Profile')

@section('nav')
    @include('parts.navs.profile')
@endsection

@section('page')

    <div class="container">

        <div class="row">

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-gears"></i><br/>Preferences
                        </a>
                    </p>
                </div>
            </div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-address-card-o"></i><br/>Contact Info
                        </a>
                    </p>
                </div>
            </div>

            <div class="w-100 home-panel-icon-divider"></div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-user-circle-o"></i><br/>Personal Info
                        </a>
                    </p>
                </div>
            </div>

            <div class="col">
                <div class="card card-body home-panel-icons blue-grey lighten-5">
                    <p class="card-text text-center">
                        <a href="#">
                            <i class="fa fa-lock"></i><br/>Reset Password
                        </a>
                    </p>
                </div>
            </div>

        </div>

    </div>

@endsection

