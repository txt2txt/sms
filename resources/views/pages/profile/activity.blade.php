@extends('layouts.dash')

@section('page-title', 'Profile')

@section('nav')
    @include('parts.navs.profile')
@endsection

@section('page')

    <div class="container">

        <table class="table table-bordered">


            <thead>
            <tr>
                <th>Date/Time</th>
                <th>IP Address</th>
                <th>Action</th>
                <th>Device</th>
            </tr>
            </thead>

            <tbody>
            @foreach($activity as $act)
                <tr>
                    <td>{{ $act->created_at }}</td>
                    <td>{{ $act->ip }}</td>
                    <td>{{ $act->event_type_id }}</td>
                    <td>{{ $act->user_agent }}</td>
                </tr>
            @endforeach
            </tbody>

        </table>

    </div>

@endsection

