<div>
    <p>Hi: {{ $user->fullName() }}</p>
    <p>Please find below your password reset link.</p>
    <p><a href="{{ $user->passwordResetUrl() }}">{{ $user->passwordResetUrl() }}</a></p>
    <p>Sincerely,</p>
    <p>{{ config('mail.from.name') }}</p>
    <p>{{ config('mail.from.address') }}</p>
</div>
