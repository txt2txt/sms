Hi: {{ $user->fullName() }}
Your account has been locked by numerous failed login attempts. If this was you trying to login, please click on the link below to unlock your account.
{{ $user->unlockUrl() }}
If you did not make these login attempts, please click on the link below the regenerate your unique login url.
{{ $user->regenerateIdUrl() }}
NEVER SHARE YOUR LOGIN URL with ANYONE.
Sincerely,
{{ config('mail.from.name') }}
{{ config('mail.from.address') }}