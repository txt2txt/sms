Hi: {{ $user->fullName() }}
Welcome to {{ config('app.name') }}. Please click the activation link below to activate your account. The below login url will not work until the account has been activated
Activation Link: {{ $user->activateUrl() }}
For security purposes, our system uses unique login urls to identify and try to ensure the security of your data. Please find below your personal unique login url. Bookmark this url for future logins.
Here is your unique login url:
Login url: {{ $user->loginUrl() }}
This is your own person url, do not share this url with anyone.
NEVER SHARE YOUR LOGIN URL with ANYONE.
Sincerely,
{{ config('mail.from.name') }}
{{ config('mail.from.address') }}

