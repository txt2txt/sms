<div>
    <p>Hi: {{ $user->fullName() }}</p>
    <p>Welcome to {{ config('app.name') }}. Please click the activation link below to activate your account. The below login url will not work until the account has been activated.</p>
    <p>Activation Link: <a href="{{ $user->activateUrl() }}">{{ $user->activateUrl() }}</a></p>
    <p>For security purposes, our system uses unique login urls to identify and try to ensure the security of your data. Please find below your personal unique login url. Bookmark this url for future logins.</p>
    <p>Here is your unique login url:</p>
    <p>Login url: <a href="{{ $user->loginUrl() }}">{{ $user->loginUrl() }}</a></p>
    <p>This is your own person url, do not share this url with anyone.</p>
    <p>NEVER SHARE YOUR LOGIN URL with ANYONE.</p>
    <p>Sincerely,</p>
    <p>{{ config('mail.from.name') }}</p>
    <p>{{ config('mail.from.address') }}</p>
</div>
