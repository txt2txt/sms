Hi: {{ $user->fullName() }}
Here is your unique login url:
{{ $user->loginUrl() }}
This is your own person url, do not share this url with anyone.
NEVER SHARE YOUR LOGIN URL with ANYONE.
Sincerely,
{{ config('mail.from.name') }} {{ config('mail.from.address') }}