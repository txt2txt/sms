<div>
    <p>Hi: {{ $user->fullName() }}</p>
    <p>Your account has been locked by numerous failed login attempts. If this was you trying to login, please click on the link below to unlock your account.</p>
    <p><a href="{{ $user->unlockUrl() }}">{{ $user->unlockUrl() }}</a></p>
    <p>If you did not make these login attempts, please click on the link below the regenerate your unique login url.</p>
    <p><a href="{{ $user->regenerateIdUrl() }}">{{ $user->regenerateIdUrl() }}</a></p>
    <p>NEVER SHARE YOUR LOGIN URL with ANYONE.</p>
    <p>Sincerely,</p>
    <p>{{ config('mail.from.name') }}</p>
    <p>{{ config('mail.from.address') }}</p>
</div>