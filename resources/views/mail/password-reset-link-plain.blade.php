Hi: {{ $user->fullName() }}
Please find below your password reset link.
{{ $user->passwordResetUrl() }}
Sincerely,
{{ config('mail.from.name') }}
{{ config('mail.from.address') }}