<div>
    <p>Hi: {{ $user->fullName() }}</p>
    <p>Here is your unique login url:</p>
    <p><a href="{{ $user->loginUrl() }}">{{ $user->loginUrl() }}</a></p>
    <p>This is your own person url, do not share this url with anyone.</p>
    <p>NEVER SHARE YOUR LOGIN URL with ANYONE.</p>
    <p>Sincerely,</p>
    <p>{{ config('mail.from.name') }}</p>
    <p>{{ config('mail.from.address') }}</p>
</div>

