<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ config('app.name') }} | @yield('page-title')</title>

    @include('parts.styles')

</head>
<body>

@include('parts.header-main')

<div class="container-fluid">

    <div class="row">

        <div class="col-md-10 offset-md-1 dash-content-container">

            <div class="jumbotron jumbotron-fluid dash-content-jumbo blue-grey lighten-5">

                <div class="container">
                    <h3 class="h3-reponsive mb-4 mt-2 font-bold text-center"></h3>
                    <p class="lead text-center">Welcome to your dashboard</p>
                </div>

            </div>

            <nav class="navbar navbar-dark blue-grey lighten-2 dash-nav-2">
                <a class="navbar-brand text-white">Hi {{ auth()->user()->fullName() }}</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dash-nav-2" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="dash-nav-2">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ routeFor('dash-home') }}">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ routeFor('profile-home') }}">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Communications</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="row">

                <div class="col-3">

                    @yield('nav')

                </div>

                <div class="col-9">

                    @yield('page')

                </div>

            </div>

        </div>

    </div>

</div>

@include('parts.footer-main')

@include('parts.scripts')

</body>
</html>