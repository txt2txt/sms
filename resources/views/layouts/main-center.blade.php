<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('site-name') | @yield('page-title')</title>

    @include('parts.styles')

</head>
<body>

    @include('parts.header-main')

    <div class="container-fluid">

        <div class="row">

            <div class="col">

                @yield('page')

            </div>

        </div>

    </div>

    @include('parts.footer-main')

    @include('parts.scripts')

</body>
</html>