<?php

use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    /**
	     * User roles array.
	     */
	    $userRoles = [
		    'Super Admin', // Application administrator
		    'Customer Service', // Customer service
		    'Admin', // Organization administrator
		    'Manager',
		    'Editor',
		    'Author',
		    'Recipient'
	    ];

	    /**
	     * Loop through entity types and seed table
	     */
	    foreach($userRoles as $name):
		    DB::table('users_roles')->insert([
			    [
				    'name' => $name,
				    'created_at' => \Carbon\Carbon::now(),
				    'updated_at' => \Carbon\Carbon::now()
			    ]
		    ]);
	    endforeach;
    }
}
