<?php

use Illuminate\Database\Seeder;

use SMS\Eloquent\Country;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $countries  = config('countries');

	    foreach($countries as $abbr => $country):
		    (new Country())->create([
			    'abbr'  => $abbr,
			    'name' => $country,
			    'active' => 0
		    ]);
	    endforeach;

	    /**
	     * Activate the United States
	     */
	    $us = (new Country)->find(232);
	    $us->country_code = 1;
	    $us->active = 1;
	    $us->save();

	    /**
	     * Activate Canada
	     */
	    $ca = (new Country)->find(39);
	    $ca->country_code = 1;
	    $ca->active = 1;
	    $ca->save();
    }
}
