<?php

use Illuminate\Database\Seeder;
use SMS\Eloquent\SecurityQuestion;

class SecurityQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$questions = config('security.questions');

		if(is_array($questions))
		{
			foreach($questions as $question)
			{
				SecurityQuestion::create([
					'question' => $question
				]);
			}
		}
    }
}
