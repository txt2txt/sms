<?php

use Illuminate\Database\Seeder;
use SMS\Enums;

class DictionarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    /**
	     * Entity types.
	     */
    	$entityTypes = [
    	    'User' => Enums\EntityTypes::$UserTypeId,
		    'Recipient' => Enums\EntityTypes::$RecipientTypeId,
		    'Recipient Group' => Enums\EntityTypes::$RecipientGroupTypeId,
		    'Message Queued' => Enums\EntityTypes::$MessageQueuedTypeId,
		    'Message Sent' => Enums\EntityTypes::$MessageSentTypeId,
		    'Message Received' => Enums\EntityTypes::$MessageReceivedTypeId,
	    ];

        $entityTypeID = DB::table('dictionary')->insertGetId([
        	'name' => 'Entity Type',
	        'created_at' => \Carbon\Carbon::now(),
	        'updated_at' => \Carbon\Carbon::now()
        ]);

	    /**
	     * Loop through entity types and seed table
	     */
	    $this->_populateDicRef($entityTypeID, $entityTypes);


	    /**
	     * Events types.
	     */
	    $eventTypes = [
		    'User Login Request' => Enums\EventTypes::$UserLoginRequest,
		    'User Auth Success' => Enums\EventTypes::$UserAuthSuccess,
		    'User Auth Fail' => Enums\EventTypes::$UserAuthFail,
		    'User Sign Up' => Enums\EventTypes::$UserSignUp,
		    'User Activated Account' => Enums\EventTypes::$UserAccountActivated,
		    'User Profile Modified' => Enums\EventTypes::$UserProfileModified,
		    'User Password Update' => Enums\EventTypes::$UserPasswordUpdate,
		    'User Regenerate Url Id' => Enums\EventTypes::$UserRegenerateUrlId,
		    'User Account Deleted' => Enums\EventTypes::$UserAccountDeleted,
		    'User Logout' => Enums\EventTypes::$UserLogout,
		    'User Password Reset Request' => Enums\EventTypes::$UserResetPasswordReq,
		    'User Password Reset' => Enums\EventTypes::$UserResetPassword,
		    'User Account Locked' => Enums\EventTypes::$UserAccountLocked,
		    'User Account Unlocked' => Enums\EventTypes::$UserAccountUnlocked
	    ];

	    $eventTypeID = DB::table('dictionary')->insertGetId([
		    'name' => 'Event Type',
		    'created_at' => \Carbon\Carbon::now(),
		    'updated_at' => \Carbon\Carbon::now()
	    ]);

	    $this->_populateDicRef($eventTypeID, $eventTypes);


	    /**
	     * Token types.
	     */
	    $tokenTypes = [
		    'Account Activation' => Enums\TokenTypes::$AccountActivation,
		    'Account Unlock' => Enums\TokenTypes::$AccountUnlock,
		    'Account Regenerate Url Id' => Enums\TokenTypes::$AccountRegenerateId,
		    'Password Reset' => Enums\TokenTypes::$PasswordReset,
	    ];

	    $tokenTypeId = DB::table('dictionary')->insertGetId([
		    'name' => 'Token Types',
		    'created_at' => \Carbon\Carbon::now(),
		    'updated_at' => \Carbon\Carbon::now()
	    ]);

	    $this->_populateDicRef($tokenTypeId, $tokenTypes);


	    /**
	     * User statuses.
	     */
	    $userStatuses = [
		    'Unactivated' => Enums\UserStatuses::$Unactivated,
		    'Active' => Enums\UserStatuses::$Active,
		    'Deactivated' => Enums\UserStatuses::$Deactivated,
		    'Suspended'  => Enums\UserStatuses::$Suspended,
		    'Locked' => Enums\UserStatuses::$Locked,
		    'Requested Password Reset' => Enums\UserStatuses::$ReqPassReset
	    ];

	    $userStatusId = DB::table('dictionary')->insertGetId([
		    'name' => 'User Statuses',
		    'created_at' => \Carbon\Carbon::now(),
		    'updated_at' => \Carbon\Carbon::now()
	    ]);

	    $this->_populateDicRef($userStatusId, $userStatuses);
    }


	/**
	 * Seed dictionary references table with values and parent id.
	 *
	 * @param $parentId
	 * @param $values
	 */
    private function _populateDicRef($parentId, $values)
    {
	    /**
	     * Loop through types and seed table
	     */
	    foreach($values as $name => $id):
		    DB::table('dictionary_references')->insert([
			    [
				    'dictionary_id' => $parentId,
				    'reference_id' => $id,
				    'reference' => $name,
				    'created_at' => \Carbon\Carbon::now(),
				    'updated_at' => \Carbon\Carbon::now()
			    ]
		    ]);
	    endforeach;
    }

}
