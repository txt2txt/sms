<?php

use Illuminate\Database\Seeder;
use SMS\Enums\UserRoles;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new SMS\Eloquent\User();
        $user->first_name = "Joshua";
        $user->last_name = "Wieczorek";
        $user->email = "joshuawieczorek@outlook.com";
        $user->password = Hash::make('yhwh1234');
        $user->phone_number = "5613325055";
        $user->country_id = 232;
        $user->url_id = uniqueUserIdentifier();
        $user->status = 1;
        $user->save();

	    $user->roles()->attach([1]);

	    $user->securityQuestions()->create([
	    	'security_question_id' => 1,
		    'answer' => encrypt('The Bible')
	    ]);

	    $user->securityQuestions()->create([
		    'security_question_id' => 2,
		    'answer' => encrypt('Kittle Rd')
	    ]);

	    $user->securityQuestions()->create([
		    'security_question_id' => 4,
		    'answer' => encrypt('Snoopy')
	    ]);

	    $user->configs()->create([
	    	'name' => 'password_set',
		    'value' => now()
	    ]);
    }
}
