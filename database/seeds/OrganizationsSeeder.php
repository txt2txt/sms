<?php

use Illuminate\Database\Seeder;

class OrganizationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$shout = new SMS\Eloquent\Organization();
    	$shout->id = 'w5rli1qi';
    	$shout->name = 'Shout of Victory';
    	$shout->save();
		$shout->configs()->create(['name'=>'APIKey', 'value'=>'this is the key']);
    }
}
