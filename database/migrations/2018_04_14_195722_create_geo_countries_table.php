<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('geo_countries', function (Blueprint $table) {
		    $table->increments('country_id');
		    $table->string('name');
		    $table->string('abbr', 5);
		    $table->integer('country_code')->nullable();
		    $table->string('notes')->nullable();
		    $table->boolean('active')->default(0);
		    $table->boolean('deleted')->default(0);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_countries');
    }
}
