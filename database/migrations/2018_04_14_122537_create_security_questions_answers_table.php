<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecurityQuestionsAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('security_questions_answers', function (Blueprint $table) {
            $table->increments('security_questions_answer_id');
            $table->integer('user_id')->unsigned();
            $table->integer('security_question_id')->unsigned();
            $table->text('answer');
            $table->timestamps();
        });

        Schema::table('security_questions_answers', function(Blueprint $table) {
        	$table->foreign('user_id')
	              ->references('user_id')
		          ->on('users')
		          ->onDelete('cascade');

	        $table->foreign('security_question_id')
	              ->references('security_question_id')
	              ->on('security_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('security_questions_answers');
    }
}
