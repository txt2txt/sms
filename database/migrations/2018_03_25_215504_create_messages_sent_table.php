<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesSentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_sent', function (Blueprint $table) {
            $table->increments('message_id');
	        $table->string('to');
	        $table->string('from');
            $table->text('body');
	        $table->string('resp_message_id')->nullable();
	        $table->string('resp_status')->nullable();
	        $table->string('resp_remaining_balance')->nullable();
	        $table->string('resp_message_price')->nullable();
	        $table->string('resp_network')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_sent');
    }
}
