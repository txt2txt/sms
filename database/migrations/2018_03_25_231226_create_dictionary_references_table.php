<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDictionaryReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionary_references', function (Blueprint $table) {
            $table->increments('dictionary_reference_id');
            $table->integer('dictionary_id')->unsigned();
	        $table->integer('reference_id');
	        $table->string('reference');
            $table->timestamps();
        });

        Schema::table('dictionary_references', function(Blueprint $table){
        	$table->foreign('dictionary_id')
		        ->references('dictionary_id')
		        ->on('dictionary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary_references');
    }
}
