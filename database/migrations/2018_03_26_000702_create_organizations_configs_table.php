<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations_configs', function (Blueprint $table) {
            $table->increments('organizations_config_id');
	        $table->integer('organization_id')->unsigned();
	        $table->string('name');
	        $table->longText('value');
            $table->timestamps();
        });

        Schema::table('organizations_configs', function(Blueprint $table) {
        	$table->foreign('organization_id')
		        ->references('organization_id')
		        ->on('organizations')
		        ->onCascade('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations_configs');
    }
}
