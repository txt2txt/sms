<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_errors', function (Blueprint $table) {
            $table->increments('log_error_id');
            $table->longText('exception');
	        $table->longText('inner_exception')->nullable();
	        $table->text('file')->nullable();
	        $table->string('line')->nullable();
	        $table->string('code', 10)->nullable();
	        $table->longText('previous')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_errors');
    }
}
