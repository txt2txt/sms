<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_received', function (Blueprint $table) {
            $table->increments('messages_received_id');
            $table->string('msisdn');
	        $table->string('to');
	        $table->string('messageId');
	        $table->text('text');
	        $table->string('type');
	        $table->string('keyword');
	        $table->string('message_timestamp');
	        $table->string('timestamp');
	        $table->string('nonce');
	        $table->string('concat');
	        $table->string('concat_ref');
	        $table->string('concat_total');
	        $table->string('concat_part');
	        $table->string('data');
	        $table->string('udh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_received');
    }
}
