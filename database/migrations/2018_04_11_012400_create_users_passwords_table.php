<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPasswordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_passwords', function (Blueprint $table) {
            $table->increments('users_password_id');
            $table->integer('user_id')->unsigned();
            $table->string('password');
            $table->timestamp('date_reset');
        });

        Schema::table('users_passwords', function(Blueprint $table) {
        	$table->foreign('user_id')
		        ->references('user_id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_passwords');
    }
}
