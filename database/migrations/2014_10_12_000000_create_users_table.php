<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('first_name', 30);
	        $table->string('last_name', 30);
            $table->string('email')->unique();
	        $table->string('password');
	        $table->string('phone_number', 15);
	        $table->integer('country_id')->unsigned();
	        $table->string('url_id', 11)->nullable();
	        $table->integer('attempts')->default(0);
	        $table->integer('status')->default(0); // 0 = Inactive, 1 = Active, 3 = Locked
	        $table->timestamp('last_login')->nullable();
	        $table->timestamp('date_locked')->nullable();
	        $table->boolean('deleted')->default(0);
            $table->timestamps();
            $table->string('remember_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
