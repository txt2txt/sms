<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipients_groups', function (Blueprint $table) {
            $table->increments('recipients_groups_id');
            $table->integer('recipient_id')->unsigned();
            $table->integer('recipient_group_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('recipients_groups', function(Blueprint $table) {
        	$table->foreign('recipient_id')
		        ->references('recipient_id')
		        ->on('recipients');

	        $table->foreign('recipient_group_id')
	              ->references('recipient_group_id')
	              ->on('recipient_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipients_groups');
    }
}
