<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCountryIdFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    /**
	     * Add country_id fk to users table.
	     */
        Schema::table('users', function(Blueprint $table) {
        	$table->foreign('country_id')
		          ->references('country_id')
		          ->on('geo_countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    /**
	     * Drop country_id fk to users table.
	     */
	    Schema::table('users', function(Blueprint $table) {
		    $table->dropForeign('users_country_id_foreign');
	    });
    }
}
