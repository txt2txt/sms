<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_configs', function (Blueprint $table) {
	        $table->increments('users_config_id');
	        $table->integer('user_id')->unsigned();
	        $table->string('name');
	        $table->longText('value');
	        $table->timestamps();
        });

        Schema::table('users_configs', function(Blueprint $table) {
        	$table->foreign('user_id')
		        ->references('user_id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_configs');
    }
}
