<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesQueuedGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_queued_groups', function (Blueprint $table) {
            $table->increments('messages_queued_group_id');
            $table->integer('messages_queued_id')->unsigned();
	        $table->integer('recipient_groups_id')->unsigned();
            $table->timestamps();
        });

	    Schema::table('messages_queued_groups', function(Blueprint $table) {
		    $table->foreign('messages_queued_group_id')
		          ->references('messages_queued_id')
		          ->on('messages_queued');

		    $table->foreign('recipient_groups_id')
		          ->references('recipient_group_id')
		          ->on('recipient_groups');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_queued_groups');
    }
}
