<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_emails', function (Blueprint $table) {
            $table->increments('logs_email_id');
	        $table->integer('user_id')->nullable();
	        $table->string('subject');
            $table->string('to');
	        $table->string('from');
	        $table->text('body');
	        $table->longText('full_message');
	        $table->boolean('deleted')->default(0);
            $table->timestamp('date_sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_emails');
    }
}
