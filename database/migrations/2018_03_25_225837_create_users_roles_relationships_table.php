<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRolesRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_roles_relationships', function (Blueprint $table) {
            $table->increments('users_roles_relationship_id');
	        $table->integer('role_id')->unsigned();
	        $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('users_roles_relationships', function(Blueprint $table){
        	$table->foreign('role_id')
		        ->references('users_role_id')
		        ->on('users_roles');

	        $table->foreign('user_id')
	              ->references('user_id')
	              ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_roles_relationships');
    }
}
