<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_users', function (Blueprint $table) {
            $table->increments('log_user_id');
            $table->integer('user_id')->unsigned();
			$table->integer('event_type_id');
	        $table->string('ip')->nullable();
			$table->text('notes')->nullable();
	        $table->text('user_agent')->nullable();
            $table->timestamps();
        });

        Schema::table('logs_users', function(Blueprint $table) {
	        $table->foreign('user_id')
	              ->references('user_id')
	              ->on('users')
	              ->onDelte('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_users');
    }
}
