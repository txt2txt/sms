<?php

namespace SMS\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use SMS\Eloquent\User;

class PasswordUsed implements Rule
{
	/**
	 * PasswordUsed constructor.
	 *
	 */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
	    // Find user by url id
	    $user = User::where('url_id', session('urlId'))->first();

	    // No user bail
	    if($user == null)
		    return false;

	    // Check if new password is same as current password.
	    if(Hash::check($value, $user->password))
		    return false;

	    // Get old passwords.
	    $oldPasswords = $user->oldPasswords()->orderBy('date_reset', 'desc')->take(10)->get();

	    // Check if password was recently used.
	    foreach($oldPasswords as $password)
	    {
		    if(Hash::check($value, $password->password))
			    return false;
	    }

	    // Otherwise all good...
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This password is one of your last 10 passwords, please choose a different one.';
    }
}
