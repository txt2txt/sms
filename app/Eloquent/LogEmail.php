<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class LogEmail extends Model
{
	/**
	 * Turn off time stamps.
	 *
	 * @var bool
	 */
	public $timestamps = false;


	/**
	 * Table name
	 *
	 * @var string
	 */
    protected $table = 'logs_emails';


    /**
     * Primary ken
     */
    protected $primaryKey = 'logs_email_id';
}
