<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class OrganizationConfigs extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'organizations_configs';

	/**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'organizations_config_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'value'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

	];
}
