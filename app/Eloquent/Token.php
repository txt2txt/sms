<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
    protected $table = 'tokens';


	/**
	 * Primary key of the table
	 *
	 * @var string
	 */
    protected $primaryKey = 'token_id';


	/**
	 * Mass-assignable fields.
	 *
	 * @var array
	 */
    protected $fillable = [
    	'user_id',
    	'type',
	    'value',
	    'active',
	    'expires',
    ];
}
