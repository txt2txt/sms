<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
	/**
	 * Table name.
	 *
	 * @var string
	 */
	protected $table = 'users_roles';

	/**
	 * Primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'users_role_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

	];
}
