<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'recipients';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'last_name',
		'phone_number',
		'country_code'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

	];
}
