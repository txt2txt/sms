<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
    protected $table = 'security_questions';


	/**
	 * Table's primary key.
	 *
	 * @var string
	 */
    protected $primaryKey = 'security_question_id';


	/**
	 * Mass-assignable fields.
	 *
	 * @var array
	 */
    protected $fillable = [
		'question'
    ];
}
