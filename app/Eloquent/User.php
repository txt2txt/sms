<?php

namespace SMS\Eloquent;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
	    'last_name',
	    'email',
	    'password',
	    'phone_number',
	    'url_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


	/**
	 * Get user's roles.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
    public function roles()
    {
	    return $this->belongsToMany( UserRoles::class, 'users_roles_relationships', 'user_id', 'role_id', 'user_id')->withTimestamps();
    }


	/**
	 * Check if user has role.
	 *
	 * @param $role
	 *
	 * @return bool
	 */
    public function hasRole($role)
    {
    	foreach($this->roles as $r):
		    if($r->name == $role)
		    	return true;
	    endforeach;

	    return false;
    }


	/**
	 * Get user's config values.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function configs()
    {
    	return $this->hasMany(UserConfigs::class, 'user_id', 'user_id');
    }


	/**
	 * User's tokens.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function tokens()
    {
    	return $this->hasMany(Token::class, 'user_id', 'user_id');
    }

	/**
	 * User's security questions.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function securityQuestions()
    {
    	return $this->hasMany(SecurityQuestionAnswer::class, 'user_id', 'user_id');
    }


	/**
	 * User's old passwords.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function oldPasswords()
    {
    	return $this->hasMany(UserPasswords::class, 'user_id', 'user_id');
    }


	/**
	 * User's emails.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function emails()
    {
    	return $this->hasMany(LogEmail::class, 'user_id', 'user_id');
    }


	/**
	 * User's activity
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function activity()
    {
    	return $this->hasMany(LogUser::class, 'user_id', 'user_id');
    }


	/**
	 * Check if user has a config value and return value or default.
	 *
	 * @param $name
	 * @param $default
	 *
	 * @return mixed|null
	 */
    public function config($name, $default)
    {
    	$config = $this->configs()->where('name', '=', $name)->first();
    	if($config != null)
    		return $config->value;
    	return $default;
    }


	/**
	 * Return user's full name.
	 *
	 * @return string
	 */
    public function fullName()
    {
    	return $this->first_name . ' ' . $this->last_name;
    }


	/**
	 * Create user's activation link
	 *
	 * @return string
	 */
    public function activateUrl()
    {
    	$token = $this->tokens()
	                  ->where('active', '=', 1)
		              ->where('type', '=', \SMS\Enums\TokenTypes::$AccountActivation)
	                  ->first();

    	return url()->route('account-activate', [$this->url_id, $token->value]);
    }

	/**
	 * Create user's login url.
	 *
	 * @return string
	 */
    public function loginUrl()
    {
		return url()->route('login', $this->url_id);
    }


	/**
	 * Create user's unlock url.
	 *
	 * @return string
	 */
	public function unlockUrl()
	{
		$token = $this->tokens()
		              ->where('active', '=', 1)
		              ->where('type', '=', \SMS\Enums\TokenTypes::$AccountUnlock)
		              ->first();

		return url()->route('unlock-account', [$this->url_id, $token->value]);
	}


	/**
	 * Create user's regenerate id url.
	 */
	public function regenerateIdUrl()
	{
		$token = $this->tokens()
		              ->where('active', '=', 1)
		              ->where('type', '=', \SMS\Enums\TokenTypes::$AccountRegenerateId)
		              ->first();

		return url()->route('regenerate-url', [$this->url_id, $token->value]);
	}


	/**
	 * Create user's password reset url.
	 */
	public function passwordResetUrl()
	{
		$token = $this->tokens()
		              ->where('active', '=', 1)
		              ->where('type', '=', \SMS\Enums\TokenTypes::$PasswordReset)
		              ->first();

		return url()->route('password-reset-email-token', [$this->url_id, $token->value]);
	}
}
