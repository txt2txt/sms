<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MessageReceived extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'messages_received';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'msisdn',
		'to',
		'text',
		'type',
		'keyword',
		'message_timestamp',
		'timestamp',
		'nonce',
		'concat',
		'concat_ref',
		'concat_total',
		'concat_part',
		'data',
		'udh'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

	];
}
