<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MessageSent extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'messages_sent';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'to',
		'from',
		'body'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

	];
}
