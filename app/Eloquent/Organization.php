<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use SMS\Enums;

class Organization extends Model
{
	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'organizations';

	/**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'organization_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

	];


	public function configs()
	{
		return $this->hasMany(OrganizationConfigs::class, 'organization_id', 'organization_id');
	}


	public function recipients()
	{
		return DB::table('organizations_relationships as or')
			->join('recipients as r', 'r.recipient_id', '=', 'or.entity_id')
			->select('r.*')
			->where('or.entity_type_id', '=', Enums\EntityTypes::$RecipientTypeId)
			->get();
	}


	public function recipientGroups()
	{
		return DB::table('organizations_relationships as or')
		         ->join('recipients_groups as rg', 'rg.recipient_id', '=', 'or.entity_id')
		         ->select('r.*')
		         ->where('or.entity_type_id', '=', Enums\EntityTypes::$RecipientTypeId)
		         ->get();
	}


	public function config($name)
	{
		foreach($this->configs as $config):

			if($config->name == $name)
				return $config->value;

		endforeach;

		return '';
	}
}
