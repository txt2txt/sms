<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestionAnswer extends Model {
	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'security_questions_answers';


	/**
	 * Table's primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'security_questions_answer_id';


	/**
	 * Mass assignable fields.
	 *
	 * @var array
	 */
	protected $fillable = [
		'security_question_id',
		'answer'
	];

	/**
	 * Question's user.
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'user_id');
	}
}
