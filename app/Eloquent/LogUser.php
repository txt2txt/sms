<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class LogUser extends Model
{
	/**
	 * Table
	 *
	 * @var string
	 */
    protected $table = 'logs_users';

	/**
	 * Mass assignable fields
	 *
	 * @var array
	 */
    protected $fillable = [
    	'user_id',
    	'event_type_id',
    	'notes'
    ];
}
