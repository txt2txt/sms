<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserPasswords extends Model
{
	/**
	 * Turn off timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * Table name
	 *
	 * @var string
	 */
	protected $table = 'users_passwords';

	/**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'users_password_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'password',
		'date_reset'
	];
}
