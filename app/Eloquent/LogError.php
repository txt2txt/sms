<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class LogError extends Model
{
	/**
	 * Table
	 *
	 * @var string
	 */
	protected $table = 'logs_errors';

	/**
	 * Mass assignable fields
	 *
	 * @var array
	 */
	protected $fillable = [
		'exception',
		'inner_exception',
		'file',
		'line',
		'code',
		'previous'
	];
}
