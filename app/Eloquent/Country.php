<?php

namespace SMS\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
	 * Table name.
	 *
	 * @var string
	 */
	protected $table = 'geo_countries';

	/**
	 * Primary key.
	 *
	 * @var string
	 */
	protected $primaryKey = 'country_id';

	/**
	 * Mass-assignable fields.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'abbr',
		'country_code',
		'notes',
		'active'
	];
}
