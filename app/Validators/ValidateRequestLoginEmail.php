<?php

namespace SMS\Validators;

use Illuminate\Http\Request;
use SMS\Contracts\AValidator;
use SMS\Exceptions\ValidationException;

class ValidateRequestLoginEmail extends AValidator
{
	/**
	 * Validate
	 *
	 * @param Request $request
	 *
	 * @return mixed|void
	 * @throws ValidationException
	 */
	public function validate(Request $request)
	{
		$this->_validate($request);
	}

	/**
	 * Private validation rules.
	 *
	 * @return array
	 */
	protected function _rules(): array
	{
		return [
			'email'     => 'required|email',
		];
	}

	/**
	 * Private validation messages.
	 *
	 * @return array
	 */
	protected function _messages(): array
	{
		return [
			'email.required' => 'Email is required.',
			'email.email' => 'Enter a valid email.'
		];
	}
}