<?php

namespace SMS\Validators;

use Illuminate\Http\Request;
use SMS\Contracts\AValidator;
use SMS\Exceptions\ValidationException;

class ValidateSignUp extends AValidator
{

	/**
	 * Validate
	 *
	 * @param Request $request
	 *
	 * @return mixed|void
	 * @throws ValidationException
	 */
	public function validate(Request $request)
	{
		$this->_validate($request);
	}

	/**
	 * Private validation rules.
	 *
	 * @return array
	 */
	protected function _rules(): array
	{
		return [
			'fname'     => 'required|min:2',
			'lname'     => 'required|min:2',
			'email'     => 'required|email|same:cemail|unique:users,email',
			'cemail'     => 'required',
			'password'  => [
				'required',
				'min:8',
				'max:100',
				'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
				'same:cpassword'
			],
			'cpassword'  => 'required'
		];
	}

	/**
	 * Private validation messages.
	 *
	 * @return array
	 */
	protected function _messages(): array
	{
		return [
			'fname.required' => 'First name is required.',
			'fname.min' => 'First name must be at least 2 characters long.',
			'lname.required' => 'First name is required.',
			'lname.min' => 'Last name must be at least 2 characters long.',
			'email.required' => 'Email is required.',
			'email.email' => 'Enter a valid email.',
			'email.unique' => 'Email already exists in our system.',
			'email.same' => 'Email addresses are not the same.',
			'cemail.required' => 'Please confirm your email.',
			'password.required' => 'Password is required.',
			'password.min' => 'Password must be between 8-50 characters long and contain upper, lower, numeric and special characters.',
			'password.max' => 'Password must be between 8-50 characters long and contain upper, lower, numeric and special characters.',
			'password.regex' => 'Password must be between 8-50 characters long and contain upper, lower, numeric and special characters.',
			'password.same' => 'Your passwords do not match.',
			'cpassword.required' => 'Please confirm your password.'
		];
	}
}