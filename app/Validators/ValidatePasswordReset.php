<?php

namespace SMS\Validators;

use Illuminate\Http\Request;
use SMS\Contracts\AValidator;
use SMS\Exceptions\ValidationException;
use SMS\Rules\PasswordUsed;

class ValidatePasswordReset extends AValidator
{

	/**
	 * Validate
	 *
	 * @param Request $request
	 *
	 * @return mixed|void
	 * @throws ValidationException
	 */
	public function validate(Request $request)
	{
		$this->_validate($request);
	}

	/**
	 * Private validation rules.
	 *
	 * @return array
	 */
	protected function _rules(): array
	{
		return [
			'password'  => [
				'required',
				'min:8',
				'max:100',
				'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
				'same:cpassword',
				new PasswordUsed
			],
			'cpassword'  => 'required'
		];
	}

	/**
	 * Private validation messages.
	 *
	 * @return array
	 */
	protected function _messages(): array
	{
		return [
			'password.required' => 'Password is required.',
			'password.min' => 'Password must be between 8-50 characters long and contain upper, lower, numeric and special characters.',
			'password.max' => 'Password must be between 8-50 characters long and contain upper, lower, numeric and special characters.',
			'password.regex' => 'Password must be between 8-50 characters long and contain upper, lower, numeric and special characters.',
			'password.same' => 'Your passwords do not match.',
			'cpassword.required' => 'Please confirm you password.'
		];
	}
}