<?php

namespace SMS\Contracts;

use Illuminate\Http\Request;
use SMS\Exceptions\ValidationException;
use Illuminate\Support\Facades\Validator;

/**
 * Validation contract.
 *
 * Class Validator
 * @package Agspp\Core\Library
 */
abstract class AValidator
{
	/**
	 * Validate method.
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException
	 */
	protected function _validate(Request $request)
	{
		// Validate input.
		$validator = Validator::make($request->all(),
			$this->_rules(),
			$this->_messages()
		);

		// If invalid throw validation exception.
		throw_if($validator->fails(), new ValidationException($validator->errors()->messages()));
	}

	/**
	 * Public validate function
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	abstract public function validate(Request $request);

	/**
	 * Private validation rules.
	 *
	 * @return array
	 */
	abstract protected function _rules(): array;

	/**
	 * Private validation messages.
	 *
	 * @return array
	 */
	abstract protected function _messages(): array;
}