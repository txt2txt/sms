<?php

namespace SMS\Contracts;

use Illuminate\Http\Request;

interface IResourceController
{
	public function Get(Request $request, $id);
	public function Create(Request $request);
	public function Update(Request $request);
}