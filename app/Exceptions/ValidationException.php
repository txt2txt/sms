<?php

namespace SMS\Exceptions;

use Exception;
use Throwable;

class ValidationException extends Exception implements Throwable
{
	/**
	 * Error messages.
	 *
	 * @var array
	 */
	private $_errors = [];

	public function __construct($messages = [], $code = 0, Throwable $previous = null)
	{
		$this->_errors = $messages;
		parent::__construct("", $code, $previous);
	}

	/**
	 * Return validation errors.
	 *
	 * @return array
	 */
	public function errors() : array
	{
		return $this->_errors;
	}
}
