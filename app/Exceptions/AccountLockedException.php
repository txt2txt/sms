<?php

namespace SMS\Exceptions;

use \Throwable;

class AccountLockedException
{
	private $_locked;

	public function __construct( string $message = "", int $code = 0, Throwable $previous = null )
	{
		$this->_locked = $message;
		parent::__construct( $message, $code, $previous );
	}

	public function errors()
	{
		return [
			'locked' => $this->_locked
		];
	}
}