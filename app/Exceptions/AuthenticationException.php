<?php

namespace SMS\Exceptions;

use Exception;
use Throwable;

class AuthenticationException extends Exception
{
	private $_error;

    public function __construct( string $message = "", int $code = 0, Throwable $previous = null )
    {
    	$this->_error = $message;
	    parent::__construct( $message, $code, $previous );
    }

    public function errors()
    {
		return [
			'error' => $this->_error
		];
    }
}
