<?php

namespace SMS\Models;

use Illuminate\Http\Request;
use SMS\Exceptions\AuthenticationException;
use SMS\Exceptions\ValidationException;
use SMS\Services\UserService;

class PasswordModel
{
	/**
	 * User repo
	 *
	 * @var UserService $_userRepository
	 */
	private $_service;


	/**
	 * SecurityModel constructor.
	 *
	 * @param UserService $userService
	 */
	public function __construct(UserService $userService)
	{
		$this->_service = $userService;
	}


	/**
	 * Send a user password reset email.
	 *
	 * @param Request $request
	 * @param $urlId
	 */
	public function sendPasswordResetEmail(Request $request, $urlId)
	{
		try
		{
			$this->_service->sendPasswordResetEmail($request, $urlId);
		}
		catch (\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}


	/**
	 * Verify password reset token.
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @throws AuthenticationException
	 */
	public function verifyPasswordResetToken(Request $request, $urlId, $token)
	{
		try
		{
			$this->_service->passwordResetVerifyToken($urlId, $token);
		}
		catch (AuthenticationException $exception)
		{
			throw $exception;
		}
		catch (\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}


	/**
	 * Reset user's password
	 *
	 * @param Request $request
	 * @param $urlId
	 *
	 * @return string
	 *
	 * @throws ValidationException
	 */
	public function passwordReset(Request $request, $urlId)
	{
		try
		{
			return $this->_service->passwordReset($request, $urlId);
		}
		catch (ValidationException $exception)
		{
			throw $exception;
		}
		catch (\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}
}