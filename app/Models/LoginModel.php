<?php

namespace SMS\Models;

use Illuminate\Http\Request;
use SMS\Exceptions\AuthenticationException;
use SMS\Exceptions\ValidationException;
use SMS\Services\UserService;

class LoginModel
{
	/**
	 * User repo
	 *
	 * @var UserService $_userRepository
	 */
	private $_service;


	/**
	 * SecurityModel constructor.
	 *
	 * @param UserService $userService
	 */
	public function __construct(UserService $userService)
	{
		$this->_service = $userService;
	}


	/**
	 * @param Request $request
	 *
	 * @throws ValidationException
	 * @throws AuthenticationException
	 *
	 * @return string
	 */
	public function login(Request $request)
	{
		try
		{
			return $this->_service->login($request);
		}
		catch (ValidationException $exception)
		{
			throw $exception;
		}
		catch(AuthenticationException $exception)
		{
			throw $exception;
		}
		catch(\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}


	/**
	 * Process request for login url
	 *
	 * @param Request $request
	 *
	 * @throws ValidationException
	 */
	public function requestLogin(Request $request)
	{
		try
		{
			$this->_service->requestLogin($request);
		}
		catch(ValidationException $exception)
		{
			throw $exception;
		}
		catch(\Throwable $exception)
		{

		}
	}


	/**
	 * Log user out.
	 *
	 * @param Request $request
	 */
	public function logout(Request $request)
	{
		try
		{
			$this->_service->logout($request);
		}
		catch (\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}
}