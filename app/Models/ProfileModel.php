<?php

namespace SMS\Models;

use Illuminate\Http\Request;
use SMS\Exceptions\AuthenticationException;
use SMS\Exceptions\ValidationException;
use SMS\Services\UserService;

class ProfileModel
{
	/**
	 * User repo
	 *
	 * @var UserService $_userRepository
	 */
	private $_service;


	/**
	 * SecurityModel constructor.
	 *
	 * @param UserService $userService
	 */
	public function __construct(UserService $userService)
	{
		$this->_service = $userService;
	}


	/**
	 * Get a user's activity.
	 *
	 * @return mixed
	 */
	public function activity()
	{
		try
		{
			return $this->_service->activity();
		}
		catch (\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}
}