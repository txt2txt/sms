<?php

namespace SMS\Models;

use Illuminate\Http\Request;
use SMS\Exceptions\AuthenticationException;
use SMS\Services\UserService;

class AccountModel
{
	/**
	 * User repo
	 *
	 * @var UserService $_userRepository
	 */
	private $_service;


	/**
	 * SecurityModel constructor.
	 *
	 * @param UserService $userService
	 */
	public function __construct(UserService $userService)
	{
		$this->_service = $userService;
	}


	/**
	 * Activate a user.
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return mixed
	 * @throws AuthenticationException
	 */
	public function activateAccount(Request $request, $urlId, $token)
	{
		try
		{
			return $this->_service->activate($request, $urlId, $token);
		}
		catch(AuthenticationException $exception)
		{
			throw $exception;
		}
		catch(\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}


	/**
	 * Unlock user.
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $urlToken
	 *
	 * @return string
	 * @throws AuthenticationException
	 */
	public function unlock(Request $request, $urlId, $urlToken): string
	{
		try
		{
			return $this->_service->unlock($request, $urlId, $urlToken);
		}
		catch(AuthenticationException $exception)
		{
			throw $exception;
		}
		catch(\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}


	/**
	 * Regenerate user's url id.
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $urlToken
	 *
	 * @return null|string
	 * @throws AuthenticationException
	 */
	public function regenerateId(Request $request, $urlId, $urlToken)
	{
		try
		{
			return $this->_service->regenerateUrlId($request, $urlId, $urlToken);
		}
		catch (AuthenticationException $exception)
		{
			throw $exception;
		}
		catch (\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}
}