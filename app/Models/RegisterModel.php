<?php

namespace SMS\Models;

use Illuminate\Http\Request;
use SMS\Exceptions\ValidationException;
use SMS\Services\UserService;

class RegisterModel
{
	/**
	 * User repo
	 *
	 * @var UserService $_userRepository
	 */
	private $_service;


	/**
	 * SecurityModel constructor.
	 *
	 * @param UserService $userService
	 */
	public function __construct(UserService $userService)
	{
		$this->_service = $userService;
	}


	/**
	 * Process user sign up.
	 *
	 * @param Request $request
	 *
	 * @return bool|\SMS\Eloquent\User
	 * @throws ValidationException
	 */
	public function signUp(Request $request)
	{
		try
		{
			$this->_service->signUp($request);

			return true;
		}
		catch (ValidationException $exception)
		{
			throw $exception;
		}
		catch(\Throwable $exception)
		{
			event('log.error', $exception);
		}
	}
}