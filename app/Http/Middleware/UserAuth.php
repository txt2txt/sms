<?php

namespace SMS\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SMS\Enums\Settings;
use SMS\Enums\SessionNames;

class UserAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param $request
	 * @param Closure $next
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
	 * @throws \Exception
	 */
    public function handle(Request &$request, Closure $next)
    {
    	// If the controller is open to the public allow access.
	    if($request->route()->controller->isPublic)
		    return $next($request);

	    // Sessions clear if user is logged out.
	    if(empty(session(SessionNames::$urlId)))
	    	return redirect(routeFor('request-login', false));

	    // If user is not logged
	    if(!Auth::check())
	    {
	    	// Route name.
	    	$routeName = $request->route()->getName();

		    // If user is not on the login page, redirect to the login page.
		    if(($routeName != Settings::$LoginRouteName) && ($routeName != Settings::$LoginProcessRouteName))
		    {
			    session([SessionNames::$redirectUrl => $request->url()]);
			    return redirect(routeFor('login'));
		    }
	    }

	    return $next($request);
    }
}
