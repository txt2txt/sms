<?php

namespace SMS\Http\Middleware;

use Closure;
use SMS\Enums\SessionNames;

class PageSessions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$routeName = $request->route()->getName();

		if(!empty($routeName))
			session([SessionNames::$PageName => $routeName]);

        return $next($request);
    }
}
