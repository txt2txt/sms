<?php

namespace SMS\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use SMS\Repositories\UserRepository;
use SMS\Eloquent\User;
use SMS\Enums\UserStatuses;
use SMS\Enums\SessionNames;

class UserVerification
{
	/**
	 * Repository
	 *
	 * @var UserRepository
	 */
	private $_repo;


	/**
	 * UserSessions constructor.
	 *
	 * @param UserRepository $repository
	 */
	public function __construct(UserRepository $repository)
	{
		$this->_repo = $repository;
	}


	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request &$request, Closure $next)
    {
	    // Get domain id
	    $urlId = $request->route('userId');

	    // If user's url id is missing, then abort.
	    if($urlId == null)
		    abort(404);

	    // Find user by url id.
	    $user = $this->_repo->byUrlId($urlId);

	    // If no user found by url id then throw 404
	    if($user == null)
		    abort(404);

	    // Check user's status before proceeding.
	    $this->_checkUserStatus($user);

	    // Set sessions.
	    $this->_setSessions($user, $urlId);

		// All good... proceed.
        return $next($request);
    }


	/**
	 * Set necessary sessions.
	 *
	 * @param $user
	 * @param $urlId
	 *
	 * @return void
	 */
    private function _setSessions($user, $urlId)
    {
	    session([SessionNames::$userId => $user->user_id]);
	    session([SessionNames::$urlId => $urlId]);
    }


	/**
	 * Check user status and abort if status is incorrect.
	 *
	 * @param User $user
	 */
	private function _checkUserStatus(User $user)
	{
		// Check if user has not yet activated his account
		if($user->status == UserStatuses::$Unactivated)
			abort(403, 'Your account is inactive. Please check your email or contact customer service to activate your account.');

		// Check if user is deactivated
		if($user->status == UserStatuses::$Deactivated)
			abort(403, 'Your account has been deactivated. Please check your email for more information or contact customer service.');

		// Check if user is suspended
		if($user->status == UserStatuses::$Suspended)
			abort(403, 'Your account is suspended. Please check your email for more information or contact customer service.');

		// Check if user is locked
		if($user->status == UserStatuses::$Locked)
			abort(403, 'Your account is locked. Please check your email for more information or contact customer service.');

		// Check if user is requested to reset password
		if($user->status == UserStatuses::$ReqPassReset && session(SessionNames::$PasswordReset) !== true)
			abort(403, 'You have requested a password reset and your account is temporarily suspended until you reset your password. Please check your email for a link to reset your password or contact customer service.');
	}
}
