<?php

namespace SMS\Http\Controllers\Account;

use Illuminate\Http\Request;
use SMS\Core\Controller;
use SMS\Models\ProfileModel;

class ProfileController extends Controller
{
	public function homeShow()
	{
		return view('pages.profile.home');
	}


	public function activityShow(ProfileModel $model)
	{
		$activity = $model->activity();
		return view('pages.profile.activity')->with(['activity' => $activity]);
	}
}