<?php

namespace SMS\Http\Controllers\Account;

use Illuminate\Http\Request;
use SMS\Core\Controller;
use SMS\Models\AccountModel;
use SMS\Exceptions\AuthenticationException;

class AccountController extends Controller
{
	/**
	 * GET | Activate
	 *
	 * Activates a user's account.
	 *
	 * @param AccountModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function activate(AccountModel $model, Request $request, $urlId, $token)
	{
		try
		{
			$url = $model->activateAccount($request, $urlId, $token);
			return redirect($url)->with('success', 'Your account is now active, you may now login.');
		}
		catch (AuthenticationException $exception)
		{
			abort(403, $exception->getMessage());
		}
	}

	/**
	 * GET | Unlock
	 *
	 * @param AccountModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function unlock(AccountModel $model, Request $request, $urlId, $token)
	{
		try
		{
			$url = $model->unlock($request, $urlId, $token);
			return redirect($url)->with('success', __('messages.ctr-notifications.account-unlocked'));
		}
		catch (AuthenticationException $exception)
		{
			return abort(403, $exception->getMessage());
		}
	}

	/**
	 * GET | RegenerateId
	 *
	 * @param AccountModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function regenerateId(AccountModel $model, Request $request, $urlId, $token)
	{
		try
		{
			$url = $model->regenerateId($request, $urlId, $token);
			return redirect($url)->with('success', __('messages.ctr-notifications.reset-login-url'));
		}
		catch (AuthenticationException $exception)
		{
			return abort(403, $exception->getMessage());
		}
	}
}