<?php

namespace SMS\Http\Controllers\Account;

use Illuminate\Http\Request;
use SMS\Core\Controller;
use SMS\Models\LoginModel;
use SMS\Exceptions\ValidationException;
use SMS\Exceptions\AuthenticationException;

/**
 * Class LoginController
 * @package SMS\Http\Controllers\Account
 */
class LoginController extends Controller
{
	/**
	 * GET | Login
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function loginShow()
	{
		return view('pages.dash.login');
	}


	/**
	 * POST Login | process login
	 *
	 * @param LoginModel $model
	 * @param Request $request
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function loginProcess(LoginModel $model, Request $request)
	{
		try
		{
			$url = $model->login($request);
			return redirect($url);
		}
		catch(ValidationException $exception)
		{
			return redirect()->back()->withInput()->withErrors($exception->errors());
		}
		catch(AuthenticationException $exception)
		{
			return redirect()->back()->withInput()->withErrors($exception->errors());
		}
	}


	/**
	 * GET | Request-Login
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function requestLoginShow()
	{
		return view('pages.request-login');
	}


	/**
	 * POST | Request-Login
	 *
	 * @param LoginModel $model
	 * @param Request $request
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function requestLoginProcess(LoginModel $model, Request $request)
	{
		try
		{
			$model->requestLogin($request);
			return redirect()->back()->with('success', __('messages.ctr-notifications.request-login-url'));
		}
		catch(ValidationException $exception)
		{
			return redirect()->back()->withInput()->withErrors($exception->errors());
		}
	}


	/**
	 * GET | Logout
	 *
	 * @param LoginModel $model
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function logout(LoginModel $model, Request $request)
	{
		$model->logout($request);
		return redirect(url()->route('home'));
	}
}