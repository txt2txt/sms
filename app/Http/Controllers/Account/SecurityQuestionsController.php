<?php

namespace SMS\Http\Controllers\Account;

use Illuminate\Http\Request;
use SMS\Core\Controller;

class SecurityQuestionsController extends Controller
{

	/**
	 * GET | SecurityQuestionsAnswer
	 *
	 * Show security questions page to reset password.
	 *
	 * @param SecurityModel $model
	 * @param Request $request
	 */
	public function resetPasswordSecurityQuestions(SecurityModel $model, Request $request)
	{

	}

	/**
	 * POST | SecurityQuestionsAnswer
	 *
	 * Process security question answers for password reset.
	 *
	 * @param SecurityModel $model
	 * @param Request $request
	 */
	public function resetPasswordSecurityQuestionsProcess(SecurityModel $model, Request $request)
	{

	}


	/**
	 * GET | SecurityQuestions
	 *
	 * Show security questions for new users to setup questions/answerers.
	 *
	 * @param SecurityModel $model
	 * @param Request $request
	 */
	public function securityQuestions(SecurityModel $model, Request $request)
	{

	}


	/**
	 * POST | SecurityQuestions
	 *
	 * Process security questions answers for new users.
	 *
	 * @param SecurityModel $model
	 * @param Request $request
	 */
	public function securityQuestionsProcess(SecurityModel $model, Request $request)
	{

	}

}