<?php

namespace SMS\Http\Controllers\Account;

use Illuminate\Http\Request;
use SMS\Core\Controller;
use SMS\Enums\Settings;
use SMS\Models\RegisterModel;
use SMS\Exceptions\ValidationException;

class RegisterController extends Controller
{
	/**
	 * GET | Sign-up
	 *
	 * @param RegisterModel $model
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function registerShow(RegisterModel $model, Request $request)
	{
		return view('pages.sign-up');
	}


	/**
	 * POST | Sign-up
	 *
	 * @param RegisterModel $model
	 * @param Request $request
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function registerProcess(RegisterModel $model, Request $request)
	{
		try
		{
			$model->signUp($request);
			return redirect()->back()->with('success', sprintf('Please check your email and click on the link activate your account. The link will expire in %s minutes!', Settings::$AccountTokenExpiresInMinutes));
		}
		catch(ValidationException $exception)
		{
			return redirect()->back()->withInput()->withErrors($exception->errors());
		}
	}



}
