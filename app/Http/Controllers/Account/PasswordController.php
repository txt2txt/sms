<?php

namespace SMS\Http\Controllers\Account;

use Illuminate\Http\Request;
use SMS\Core\Controller;
use SMS\Models\PasswordModel;
use SMS\Exceptions\ValidationException;
use SMS\Exceptions\AuthenticationException;

class PasswordController extends Controller
{
	/**
	 * GET | Password-Forgot
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 * @param $urlId
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function passwordForgot(PasswordModel $model, Request $request, $urlId)
	{
		return view('pages.dash.password-forgot');
	}


	/**
	 * GET | Reset Password By Email
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function resetPasswordByEmail(PasswordModel $model, Request $request)
	{
		return view('pages.dash.password-reset-by-email');
	}


	/**
	 * POST | Reset Password By Email
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 * @param $urlId
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function resetPasswordByEmailProcess(PasswordModel $model, Request $request, $urlId)
	{
		$model->sendPasswordResetEmail($request, $urlId);
		return redirect()->back()->with('success', __('messages.ctr-notifications.password-reset-request'));
	}


	/**
	 * GET | Password Reset
	 *
	 * Show password reset form.
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function resetPassword(PasswordModel $model, Request $request, $urlId, $token)
	{
		try
		{
			$model->verifyPasswordResetToken($request, $urlId, $token);
			return view('pages.dash.password-reset');
		}
		catch (AuthenticationException $exception)
		{
			return abort(403, $exception->getMessage());
		}
	}


	/**
	 * POST | Password Reset
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function resetPasswordProcess(PasswordModel $model, Request $request, $urlId, $token)
	{
		try
		{
			$url = $model->passwordReset($request, $urlId);
			return redirect($url)->with('success', __('messages.ctr-notifications.password-reset-success'));
		}
		catch (ValidationException $exception)
		{
			return redirect()->back()->withErrors($exception->errors());
		}
	}


	/**
	 * GET | Password Reset from Profile
	 *
	 * Show password reset form.
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function resetPasswordFromProfileShow(PasswordModel $model, Request $request, $urlId)
	{
		try
		{
			return view('pages.profile.password-reset');
		}
		catch (AuthenticationException $exception)
		{
			return abort(403, $exception->getMessage());
		}
	}


	/**
	 * POST | Password Reset
	 *
	 * @param PasswordModel $model
	 * @param Request $request
	 * @param $urlId
	 * @param $token
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function resetPasswordFromProfileProcess(PasswordModel $model, Request $request, $urlId, $token)
	{
		try
		{
			$url = $model->passwordReset($request, $urlId);
			return redirect($url)->with('success', __('messages.ctr-notifications.password-reset-success'));
		}
		catch (ValidationException $exception)
		{
			return redirect()->back()->withErrors($exception->errors());
		}
	}
}