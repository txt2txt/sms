<?php

namespace SMS\Http\Controllers;

use Illuminate\Http\Request;
use SMS\Core\Controller;


class HomeController extends Controller
{
	/**
	 * HomeController constructor.
	 */
	public function __construct()
	{
		// Set controller to be publicly visible.
		$this->isPublic = true;
	}

	/**
	 * GET : Home Page
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
    {
    	session()->flush();
	    return view('pages.home');
    }
}
