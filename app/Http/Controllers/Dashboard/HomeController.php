<?php

namespace SMS\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use SMS\Core\Controller;

class HomeController extends Controller
{
    public function index(Request $request)
    {
		return view('pages.dash.home');
    }
}
