<?php

namespace SMS\Entities;

class UserLog
{
	public $userId;
	public $eventTypeId;
	public $ip;
	public $notes;
}