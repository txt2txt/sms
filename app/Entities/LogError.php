<?php

namespace SMS\Entities;

use SMS\Contracts\IPopulatable;

class LogError implements IPopulatable
{
	public $exception;
	public $innerException;
	public $previous;
	public $file;
	public $line;
	public $code;
	public $timestamp;

	public function Populate(\Exception $exception)
	{
		$this->exception = $exception->getMessage();
		$this->innerException = $exception->getTraceAsString();
		$this->file = $exception->getFile();
		$this->line = $exception->getLine();
		$this->code = $exception->getCode();
		$this->previous = $exception->getPrevious();
		$this->timestamp = \Carbon\Carbon::now();
	}
}