<?php

namespace SMS\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use SMS\Eloquent\User;

class PasswordResetRequest extends Mailable
{
    use Queueable, SerializesModels;

	public $user;

	/**
	 * PasswordResetRequest constructor.
	 *
	 * @param User $user
	 */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(sprintf('[%s] Password Reset Link', config('app.name')))
			        ->view('mail.password-reset-link')
			        ->text('mail.password-reset-link-plain');
    }
}
