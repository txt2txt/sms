<?php

namespace SMS\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use SMS\Eloquent\User;

class AccountLocked extends Mailable
{
    use Queueable, SerializesModels;

	public $user;

	/**
	 * AccountLocked constructor.
	 *
	 * @param User $user
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	    return $this->subject(sprintf('[%s] Your Account Has Been Locked', config('app.name')))
		            ->view('mail.account-locked')
	                ->text('mail.account-locked-plain');
    }
}
