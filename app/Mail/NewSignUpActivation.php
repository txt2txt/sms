<?php

namespace SMS\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use SMS\Eloquent\User;

class NewSignUpActivation extends Mailable
{
    use Queueable, SerializesModels;

	public $user;

	/**
	 * NewSignUpActivation constructor.
	 *
	 * @param User $user
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	    return $this->subject(sprintf('[%s] Account Activation', config('app.name')))
	                ->view('mail.sign-up')
	                ->text('mail.sign-up-plain');
    }
}
