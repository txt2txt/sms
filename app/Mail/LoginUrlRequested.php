<?php

namespace SMS\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use SMS\Eloquent\User;

class LoginUrlRequested extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

	/**
	 * LoginUrlRequested constructor.
	 *
	 * @param User $user
	 */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(sprintf('[%s] Requested Login URL', config('app.name')))
	                ->view('mail.login-url-request')
	                ->text('mail.login-url-request-plain');
    }
}
