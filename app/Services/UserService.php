<?php

namespace SMS\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use SMS\Repositories\UserRepository;
use SMS\Validators\ValidateLoginCredentials;
use SMS\Validators\ValidateRequestLoginEmail;
use SMS\Validators\ValidateSignUp;
use SMS\Validators\ValidatePasswordReset;
use SMS\Mail\LoginUrlRequested;
use SMS\Mail\AccountLocked;
use SMS\Mail\NewSignUpActivation;
use SMS\Mail\PasswordResetRequest;
use SMS\Enums;
use SMS\Eloquent\User;
use SMS\Exceptions\AuthenticationException;
use SMS\Core\Traits\TokenHelper;

class UserService
{
	/**
	 * Traits to use.
	 */
	use TokenHelper;

	/**
	 * Repository.
	 *
	 * @var UserRepository
	 */
	private $_repo;


	/**
	 * Token service.
	 *
	 * @var TokenService
	 */
	private $_tokenService;


	/**
	 * UserService constructor.
	 *
	 * @param UserRepository $repository
	 * @param TokenService $tokenService
	 */
	public function __construct(UserRepository $repository, TokenService $tokenService)
	{
		$this->_repo = $repository;
		$this->_tokenService = $tokenService;
	}


	/**
	 * Find user by url id using urlId session
	 *
	 * @return User
	 */
	public function findByUrlId() : User
	{
		return $this->_repo->byUrlId(session('urlId'));
	}


	/**
	 * Handles user sign-up
	 *
	 * @param Request $request
	 *
	 * @return User
	 * @throws \SMS\Exceptions\ValidationException
	 */
	public function signUp(Request $request) : User
	{
		// Validate request
		(new ValidateSignUp)->validate($request);

		// Create user
		$user = $this->_repo->signUp($request);

		// Set password
		$this->_repo->setPassword($user, $request->input('password'));

		// Update password set config date
		$this->_repo->updatePasswordSetDate($user);

		// Create user's activation token
		$this->_tokenService->create($user, Enums\TokenTypes::$AccountActivation, now()->addMinutes(Enums\Settings::$AccountTokenExpiresInMinutes));

		// Add user's admin role.
		$this->_repo->syncRoles($user, [Enums\UserRoles::$Admin]);

		// Send registration email to user
		Mail::to($user)
		    ->send(new NewSignUpActivation($user));

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserSignUp]);

		// Return user
		return $user;
	}


	/**
	 * Handles user activation
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $urlToken
	 *
	 * @return string
	 * @throws AuthenticationException
	 */
	public function activate(Request $request, $urlId, $urlToken) : string
	{
		// Get user
		$user = $this->_repo->byUrlId($urlId);

		// Verify token
		$this->verifyToken($this->_tokenService, $user, $urlToken, Enums\TokenTypes::$AccountActivation,
			now()->addMinutes(-Enums\Settings::$AccountTokenExpiresInMinutes), 'messages.token.activation');

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserAccountActivated]);

		// Deactivate user's tokens
		$this->_tokenService->deactivateByUser($user);

		// Activate and return user.
		return $this->_repo->activate($user);

	}


	/**
	 * Handle user login url request
	 *
	 * @param Request $request
	 *
	 * @throws \SMS\Exceptions\ValidationException
	 */
	public function requestLogin(Request $request)
	{
		(new ValidateRequestLoginEmail())->validate($request);

		$user = $this->_repo->byEmail($request->input('email'));

		if($user == null)
			return;

		Mail::to($user)
		    ->send(new LoginUrlRequested($user));

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserLoginRequest]);
	}


	/**
	 * Handle user login
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 * @throws AuthenticationException
	 * @throws \SMS\Exceptions\ValidationException
	 */
	public function login(Request $request) : string
	{
		(new ValidateLoginCredentials())->validate($request);

		$credentials = [
			'email' => $request->input('email'),
			'password' => $request->input('password'),
			'url_id' => $request->route('userId'),
			'user_id' => session('userId'),
			'status' => Enums\UserStatuses::$Active,
			'deleted' => 0
		];

		if(!Auth::attempt($credentials, false))
		{
			event('user.login.fail');
			throw new AuthenticationException(__('messages.auth.failed'));
		}

		// Check if user needs to change password.
		if(now()->addDays(-Enums\Settings::$PasswordResetTimeInDays) > Auth::user()->config('password_set', 0))
			session(['password_needs_to_be_reset' => true]);

		// Check if user needs to set his security questions.
		//if(Auth::user()->securityQuestions()->get()->count() != 3)
		//	session(['user_needs_to_set_security_questions' => true]);

		// Log activity
		event('user.login.success', Auth::user());

		$url = (session(Enums\SessionNames::$redirectUrl) != null && session(Enums\SessionNames::$redirectUrl) != '') ?
			session(Enums\SessionNames::$redirectUrl) :
			routeFor('dash-home');

		// Forget the redirect url
		//session()->forget(Enums\SessionNames::$redirectUrl);

		// Log out other sessions.
		Auth::logoutOtherDevices($request->input('password'));

		return $url;
	}


	/**
	 * Handle locking a user out
	 *
	 * @param User $user
	 */
	public function lock(User $user)
	{
		// Delete failed attempts session.
		session()->forget(Enums\Settings::$FailedLoginAttemptsRemaining);

		// Lock user
		$this->_repo->lock($user);

		// Create unlock token
		$this->_tokenService->create($user, Enums\TokenTypes::$AccountUnlock, now()->addMinutes(Enums\Settings::$AccountTokenExpiresInMinutes));

		// Create token to regenerate url id
		$this->_tokenService->create($user, Enums\TokenTypes::$AccountRegenerateId, now()->addMinutes(Enums\Settings::$AccountTokenExpiresInMinutes));

		// Send email to user
		Mail::to($user)
		    ->send(new AccountLocked($user));

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserAccountLocked]);
	}


	/**
	 * Handle unlocking user
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $urlToken
	 *
	 * @return string
	 * @throws AuthenticationException
	 */
	public function unlock(Request $request, $urlId, $urlToken) : string
	{
		// Get user
		$user = $this->_repo->byUrlId($urlId);

		// Verify token
		$this->verifyToken($this->_tokenService, $user, $urlToken, Enums\TokenTypes::$AccountUnlock,
			now()->addMinutes(-Enums\Settings::$AccountTokenExpiresInMinutes), 'messages.token.account-unlock');

		// All is good so proceed....

		// Remove failed login attempts session
		session()->remove(Enums\Settings::$FailedLoginAttemptsRemaining);

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserAccountUnlocked]);

		// Deactivate token
		$this->_tokenService->deactivateByUser($user);

		// Unlock and return redirect url
		return $this->_repo->unlock($user);
	}


	/**
	 * Handle user's url regeneration
	 *
	 * @param Request $request
	 * @param $urlId
	 * @param $urlToken
	 *
	 * @return null|string
	 * @throws AuthenticationException
	 */
	public function regenerateUrlId(Request $request, $urlId, $urlToken) : ?string
	{
		$user = $this->_repo->byUrlId($urlId);

		// Verify token
		$this->verifyToken($this->_tokenService, $user, $urlToken, Enums\TokenTypes::$AccountRegenerateId,
			now()->addMinutes(-Enums\Settings::$AccountTokenExpiresInMinutes), 'messages.token.invalid-url');

		if($user == null)
			return null;

		// Regenerate user's url id
		$url = $this->_repo->regenerateId($user);

		Mail::to($user)
		    ->send(new LoginUrlRequested($user));

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserRegenerateUrlId]);

		// Deactivate token
		$this->_tokenService->deactivateByUser($user);

		// Return url
		return $url;
	}


	/**
	 * Handle user logout
	 *
	 * @param Request $request
	 */
	public function logout(Request $request)
	{
		// Log activity
		event('log.activity', [Auth::user(), Enums\EventTypes::$UserLogout]);

		// Log user out
		Auth::logout();
	}


	/**
	 * Send user's password reset email.
	 *
	 * @param Request $request
	 * @param $urlId
	 */
	public function sendPasswordResetEmail(Request $request, $urlId)
	{
		// Verify email was passed in request.
		if($request->input('email') == null || $request->input('email') == '')
			return;

		// Find user by email and url id.
		$user = $this->_repo->byUrlIdAndEmail($urlId, $request->input('email'));

		// Got user - all good then proceed...
		if($user != null)
		{
			// Create token
			$this->_tokenService->create($user, Enums\TokenTypes::$PasswordReset, now()->addMinutes(Enums\Settings::$AccountTokenExpiresInMinutes));

			// Send registration email to user
			Mail::to($user)
			    ->send(new PasswordResetRequest($user));

			// Update user status to password - this will prevent login.
			$this->_repo->updateUserStatus($user, Enums\UserStatuses::$ReqPassReset);

			// Set user reset password session to true.
			session([Enums\SessionNames::$PasswordReset => true]);

			// Log activity
			event('user.log.activity', [$user, Enums\EventTypes::$UserResetPasswordReq]);
		}
	}


	/**
	 * Verify user's password reset token.
	 *
	 * @param $urlId
	 * @param $token
	 *
	 * @throws AuthenticationException
	 */
	public function passwordResetVerifyToken($urlId, $token)
	{
		// Find user.
		$user = $this->_repo->byUrlId($urlId);

		// Verify token
		$this->verifyToken($this->_tokenService, $user, $token, Enums\TokenTypes::$PasswordReset,
			now()->addMinutes(-Enums\Settings::$AccountTokenExpiresInMinutes), 'messages.token.password-reset');
	}


	/**
	 * Reset user's password
	 *
	 * @param Request $request
	 * @param $urlId
	 *
	 * @return string
	 *
	 * @throws \SMS\Exceptions\ValidationException
	 */
	public function passwordReset(Request $request, $urlId)
	{
		// Validate request.
		(new ValidatePasswordReset)->validate($request);

		// Find user.
		$user = $this->_repo->byUrlId($urlId);

		// Update user password_set config value.
		$time = $this->_repo->updatePasswordSetDate($user);

		// Save user's password to history.
		$this->_repo->saveCurrentPasswordHistory($user, $time);

		// Update user's password
		$this->_repo->setPassword($user, $request->input('password'));

		// Deactivate user's tokens.
		$this->_tokenService->deactivateByUser($user);

		// Reset user's status to active
		$this->_repo->updateUserStatus($user, Enums\UserStatuses::$Active);

		// Log activity
		event('user.log.activity', [$user, Enums\EventTypes::$UserResetPassword]);

		// Remove password reset session
		session()->forget(Enums\SessionNames::$PasswordReset);

		// Return user's login url.
		return $user->loginUrl();
	}


	/*
    |--------------------------------------------------------------------------
    | Methods Designed For Use By Events
    |--------------------------------------------------------------------------
    |
    | These methods are typically accessed by events and therefore
	| already have a user object passed to them.
    |
    */


	/**
	 * Process successful login
	 *
	 * @param User $user
	 */
	public function processLoginSuccess(User $user)
	{
		session()->remove(Enums\Settings::$FailedLoginAttemptsRemaining);

		$this->_repo->successfullyLoggedIn($user);

		event('user.log.activity', [$user, Enums\EventTypes::$UserAuthSuccess]);
	}


	/**
	 * Process failed login attempts
	 *
	 * This is generally accessed by events
	 */
	public function processLoginFailed()
	{
		// Find user
		$user = $this->findByUrlId();

		// Increment user's login attempts.
		$this->_repo->incrementLoginAttempts($user);

		// If failed login attempt session already exists then decrement it
		if(session()->has(Enums\Settings::$FailedLoginAttemptsRemaining))
		{
			session()->decrement(Enums\Settings::$FailedLoginAttemptsRemaining);

			if(session(Enums\Settings::$FailedLoginAttemptsRemaining)  < 1)
				$this->lock($user);
		}
		// Otherwise create failed login attempts
		else
			session([Enums\Settings::$FailedLoginAttemptsRemaining => (Enums\Settings::$FailedLoginMaxAttempts - 1)]);

		event('user.log.activity', [$user, Enums\EventTypes::$UserAuthFail]);
	}


	/**
	 * Find user's activity.
	 *
	 * @return mixed
	 */
	public function activity()
	{
		$userId = $this->findByUrlId();
		return $this->_repo->activity($userId);
	}
}