<?php

namespace SMS\Services;

use SMS\Repositories\TokenRepository;
use SMS\Eloquent\User;
use SMS\Eloquent\Token;

class TokenService
{
	/**
	 * Repository
	 *
	 * @var TokenRepository
	 */
	private $_repo;

	/**
	 * TokenService constructor.
	 *
	 * @param TokenRepository $repository
	 */
	public function __construct(TokenRepository $repository)
	{
		$this->_repo = $repository;
	}


	/**
	 * @param User $user
	 * @param $type
	 * @param $expires
	 *
	 * @return Token
	 */
	public function create(User $user, $type, $expires) : Token
	{
		return $this->_repo->create($user, $type, $expires);
	}


	/**
	 * Find token.
	 *
	 * @param string $value
	 *
	 * @return Token
	 */
	public function find(string $value) : ?Token
	{
		return $this->_repo->find($value);
	}


	/**
	 * Verify token is valid
	 *
	 * @param Token $token
	 * @param $userId
	 * @param $expires
	 * @param $tokenType
	 *
	 * @return bool
	 */
	public function verify(Token $token, $userId, $expires, $tokenType) : bool
	{
		$isValid = true;

		// Verify token is of correct type
		if($token->type != $tokenType)
			$isValid = false;

		// Verify token is active
		if($token->active = 0)
			$isValid = false;

		// Verify token is not expired
		if($expires > $token->expires)
			$isValid = false;

		// Verify token's user id is same as the user using the token
		if($token->user_id != $userId)
		{
			event('log.error', new \Exception("{$userId} was using token {$token->token_id} associated with {$token->user_id}!"));
			$isValid = false;
		}

		// Return verification response
		return $isValid;
	}


	/**
	 * Deactivate all tokens of a user
	 *
	 * @param User $user
	 */
	public function deactivateByUser(User $user)
	{
		$user->tokens()
		     ->where('active', '=', 1)
		     ->update(['active' => 0, 'deleted' => 1]);
	}


	/**
	 * Deactivate token by value
	 *
	 * @param string $tokenValue
	 */
	public function deactivateByValue(string $tokenValue)
	{
		$token = $this->_repo->find($tokenValue);
		$this->deactivate($token);
	}


	/**
	 * Deactivate token
	 *
	 * @param Token $token
	 */
	public function deactivate(Token $token)
	{
		$this->_repo->deactivate($token);
	}
}