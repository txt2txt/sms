<?php

/**
 * Convert data to entity.
 *
 * @param $class
 * @param $data
 *
 * @return mixed
 */
function toEntity($class, $data)
{
	return (new $class())->Populate($data);
}


/**
 * Generate db unique organizations identifiers for urls.
 *
 * @return string
 */
function uniqueOrganizationIdentifier()
{
	$id     = str_random(8);
	$org    = (new \SMS\Eloquent\Organization())->where('id', '=', $id)->get();
	if($org->Count() > 0)
		return uniqueOrganizationIdentifier();
	return $id;
}

/**
 * Generate db unique user identifiers for urls.
 *
 * @return string
 */
function uniqueUserIdentifier()
{
	$id     = strtolower(str_random(8));
	$org    = (new \SMS\Eloquent\User())->where('url_id', '=', $id)->get();
	if($org->Count() > 0)
		return uniqueUserIdentifier();
	return $id;
}


/**
 * Generate route url with uri id
 *
 * @param $route
 * @param bool $userId
 *
 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
 */
function routeFor($route, $userId=true)
{
	if(!$userId)
		return url($route);

	return url()->route($route, session(\SMS\Enums\SessionNames::$urlId));
}


/**
 * Generate types array.
 *
 * @param $typeClassName
 *
 * @return array
 */
function generateTypesArray($typeClassName)
{
	try
	{
		$classString    = sprintf("%s\%s", 'Agspp\Enums', $typeClassName);

		$returnArray    = [
			'name'  => generateTypeName($typeClassName),
			'values'=> []
		];

		$class = new ReflectionClass($classString);

		foreach($class->getProperties() as $property)
		{
			$returnArray['values'][] = [
				'desc'  => $property->name,
				'value' => $property->getValue()
			];
		}

		return $returnArray;
	}
	catch (\Throwable $exception)
	{
		return null;
	}
}


/**
 * Generate type name from class name.
 *
 * @param $name
 *
 * @return string
 */
function generateTypeName($name): string
{
	$nameString = '';
	$nameParts  = preg_split('/(?=[A-Z])/',$name);

	foreach($nameParts as $part)
	{
		$nameString .= ' ' . $part;
	}

	return trim($nameString, ' ');
}


/**
 * Check page name against session.
 *
 * @param $name
 *
 * @return bool
 */
function pageName($name)
{
	if(session(\SMS\Enums\SessionNames::$PageName) == $name)
		return true;
	return false;
}
