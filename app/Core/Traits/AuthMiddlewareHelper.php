<?php

namespace SMS\Core\Traits;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SMS\Enums\UserStatuses;
use SMS\Eloquent\User;

trait AuthMiddlewareHelper
{
	/**
	 * Handle user urls.
	 *
	 * @param $id
	 * @param $request
	 * @param Closure $next
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
	 * @throws \Exception
	 */
	protected function handleUserRequest(Request &$request, Closure &$next)
	{

	}


	/**
	 * Check if url has keywords to allow to pass the auth handler.
	 *
	 * @param $url
	 *
	 * @return bool
	 */
	private function _urlHasKeywords($url) : bool
	{
		$keywords = config('security.url-allowed-keywords');

		foreach($keywords as $keyword)
		{
			if(strpos($url, $keyword) !== false)
				return true;
		}

		return false;
	}
}