<?php

namespace SMS\Core\Traits;

use SMS\Services\TokenService;
use SMS\Eloquent\User;
use SMS\Exceptions\AuthenticationException;

trait TokenHelper
{
	/**
	 * Verify token is valid
	 *
	 * @param TokenService $service
	 * @param User $user
	 * @param $urlToken
	 * @param $tokenType
	 * @param $expires
	 * @param $msgKey
	 *
	 * @throws AuthenticationException
	 */
	public function verifyToken(TokenService $service, User $user, $urlToken, $tokenType, $expires, $msgKey)
	{
		// Get token
		$token = $service->find($urlToken);

		// Verify user and token exist
		if($user == null || $token == null)
			throw new AuthenticationException(__($msgKey));

		// Verify token
		$isValid = $service->verify($token, $user->user_id, $expires, $tokenType);

		if(!$isValid)
			throw new AuthenticationException(__($msgKey));
	}
}