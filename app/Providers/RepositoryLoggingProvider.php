<?php

namespace SMS\Providers;

use Illuminate\Support\ServiceProvider;
use SMS\Repositories\LoggingRepository;
use SMS\Eloquent\LogError;
use SMS\Eloquent\LogUser;

class RepositoryLoggingProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind('SMS\Repositories\Contracts\ILoggingRepository', function($app)
	    {
		    return new LoggingRepository(new LogError(), new LogUser());
	    });
    }
}
