<?php

namespace SMS\Providers;

use Illuminate\Support\ServiceProvider;
use SMS\Repositories\UserRepository;
use SMS\Eloquent\User;

class RepositoryUserProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind('SMS\Repositories\Contracts\IUserRepository', function($app)
	    {
		    return new UserRepository(new User());
	    });
    }
}
