<?php

namespace SMS\Events\Handlers;

use Illuminate\Mail\Events\MessageSending;
use Throwable;
use SMS\Repositories\LoggingRepository;
use SMS\Eloquent\User;

class LoggingHandler
{
	/**
	 * Repository
	 *
	 * @var LoggingRepository
	 */
	private $_repository;


	/**
	 * UserLogsHandler constructor.
	 *
	 * @param LoggingRepository $repository
	 */
	public function __construct(LoggingRepository $repository)
	{
		$this->_repository = $repository;
	}


	/**
	 * Log errors and exceptions.
	 *
	 * @param Throwable $throwable
	 */
	public function logError(Throwable $throwable)
	{
		$this->_repository->logError($throwable);
	}


	/**
	 * Log user activity
	 *
	 * @param User $user
	 * @param int $eventType
	 * @param string $notes
	 */
	public function logActivity(User $user, int $eventType, string $notes="")
	{
		$this->_repository->logActivity($user, $eventType, $notes);
	}


	/**
	 * Log emails being sent
	 *
	 * @param MessageSending $message
	 */
	public function logEmail(MessageSending $message)
	{
		$userId = 0;
		$subject = $message->message->getSubject();
		$to = array_keys($message->message->getTo())[0];
		$from = array_keys($message->message->getFrom())[0];
		$body = $message->message->getChildren()[0]->getBody();
		$fullMessage = $message->message->toString();
		$dateSent = now();

		if(isset($message->data['user']))
			$userId = $message->data['user']->user_id;

		$this->_repository->logEmail($userId, $subject, $to, $from, $body, $fullMessage, $dateSent);
	}
}