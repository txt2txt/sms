<?php

namespace SMS\Events\Handlers;

use SMS\Services\UserService;
use SMS\Enums\EventTypes;
use SMS\Eloquent\User;

/**
 * Handle user related events.
 *
 * Class UserEventHandler
 * @package SMS\Events\Handlers
 */
class UserEventHandler
{
	/**
	 * User repository.
	 *
	 * @var UserService $_service
	 */
	private $_service;

	/**
	 * UserEventHandler constructor.
	 *
	 * @param UserService $service
	 */
	public function __construct(UserService $service)
	{
		$this->_service = $service;
	}


	/**
	 * Handle user login event
	 *
	 * @param $user
	 */
	public function handleLoginSuccess($user)
	{
		$this->_service->processLoginSuccess($user);
	}


	/**
	 * Handle user failed event
	 */
	public function handleLoginFailed()
	{
		$this->_service->processLoginFailed();
	}
}