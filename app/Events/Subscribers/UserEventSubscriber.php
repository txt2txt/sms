<?php

namespace SMS\Events\Subscribers;

/**
 * This class is used to subscribe to user-related events.
 *
 * Class UserEventSubscriber
 * @package SMS\Events\Subscribers
 */
class UserEventSubscriber
{
	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		// Listen for user login request
		$events->listen(
			'user.request-login',
			'SMS\Events\Handlers\UserEventHandler@handleLoginRequest'
		);

		// Listen for user login
		$events->listen(
			'user.login.success',
			'SMS\Events\Handlers\UserEventHandler@handleLoginSuccess'
		);

		// Listen for user failed login
		$events->listen(
			'user.login.fail',
			'SMS\Events\Handlers\UserEventHandler@handleLoginFailed'
		);

		// Listen for user logout
		$events->listen(
			'user.logout',
			'SMS\Events\Handlers\UserEventHandler@handleLogout'
		);

		// Listen for user lockout
		$events->listen(
			'user.lockout',
			'SMS\Events\Handlers\UserEventHandler@handleLockout'
		);


		// Listen for user password reset
		$events->listen(
			'user.password-reset',
			'SMS\Events\Handlers\UserEventHandler@handlePasswordReset'
		);


		// Listen for user profile edit
		$events->listen(
			'user.profile-edit',
			'SMS\Events\Handlers\UserEventHandler@handleProfileEdit'
		);

		// Listen for user sign up
		$events->listen(
			'user.sign-up',
			'SMS\Events\Handlers\UserEventHandler@handleUserSignUp'
		);
	}
}

