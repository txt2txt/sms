<?php

namespace SMS\Events\Subscribers;

class LoggingSubscriber
{
	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		// Listen for email event
		$events->listen(
			'Illuminate\Mail\Events\MessageSending',
			'SMS\Events\Handlers\LoggingHandler@logEmail'
		);

		// Listen for user log activity event
		$events->listen(
			'log.error',
			'SMS\Events\Handlers\LoggingHandler@logError'
		);

		// Listen for user log activity event
		$events->listen(
			'user.log.activity',
			'SMS\Events\Handlers\LoggingHandler@logActivity'
		);
	}
}