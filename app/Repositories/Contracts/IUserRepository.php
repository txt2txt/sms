<?php

namespace SMS\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface UserRepositoryInterface
 * @package SMS\Repositories
 */
interface IUserRepository
{
	/**
	 * Find user by id.
	 *
	 * @param string $id
	 *
	 * @return null|Model
	 */
	public function byId(string $id) : ?Model;


	/**
	 * Find user by url id.
	 *
	 * @param string $id
	 *
	 * @return null|Model
	 */
	public function byUrlId(string $id) : ?Model;


	/**
	 * Find user by email.
	 *
	 * @param string $email
	 *
	 * @return null|Model
	 */
	public function byEmail(string $email) : ?Model;


	/**
	 * Find user by phone number.
	 *
	 * @param string $countryCode
	 * @param string $number
	 *
	 * @return null|Model
	 */
	public function byPhone(string $countryCode, string $number) : ?Model;


	/**
	 * Update user's successful login.
	 *
	 * @param Model $user
	 *
	 * @return mixed
	 */
	public function loginSuccess(Model $user);


	/**
	 * Update user's failed login attempt.
	 *
	 * @param Model $user
	 *
	 * @return mixed
	 */
	public function loginFailed(Model $user);


	/**
	 * Lock user out.
	 *
	 * @param Model $user
	 *
	 * @return mixed
	 */
	public function lock(Model $user);


	/**
	 * Unlock user.
	 *
	 * @param Model $user
	 *
	 * @return mixed
	 */
	public function unlock(Model $user);


	/**
	 * Regenerate user's url id.
	 *
	 * @param string $urlId
	 *
	 * @return string
	 */
	public function regenerateId(string $urlId) : string;

	/**
	 * Request user's login url.
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function requestLogin(Request $request);


	/**
	 * Try to log user in.
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function login(Request $request);


	/**
	 * User requested password reset.
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function passwordResetRequest(Request $request);


	/**
	 * User reset password.
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function passwordReset(Request $request);


	/**
	 * User log out.
	 *
	 * @return mixed
	 */
	public function logout();
}