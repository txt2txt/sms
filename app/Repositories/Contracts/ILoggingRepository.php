<?php

namespace SMS\Repositories\Contracts;

use SMS\Eloquent\User;
use \Throwable;

interface ILoggingRepository
{
	/**
	 * Log errors
	 *
	 * @param Throwable $throwable
	 *
	 * @return void
	 */
	public function logError(Throwable $throwable);

	/**
	 * Log user events.
	 *
	 * @param User $log
	 * @param int $eventType
	 * @param string $notes
	 *
	 * @return void
	 */
	public function logActivity(User $log, int $eventType, string $notes="");
}