<?php

namespace SMS\Repositories;

use SMS\Eloquent\Token;
use SMS\Eloquent\User;

class TokenRepository
{
	/**
	 * Token entity.
	 *
	 * @var Token
	 */
	private $_token;

	/**
	 * TokenRepository constructor.
	 *
	 * @param Token $token
	 */
	public function __construct(Token $token)
	{
		$this->_token = $token;
	}


	/**
	 * Create user's token.
	 *
	 * @param User $user
	 * @param $type
	 * @param $expires
	 *
	 * @return Token
	 */
	public function create(User $user, $type, $expires)
	{
		$token = $user->tokens()->create([
			'type' => $type,
			'value' => str_random(20),
			'active' => 1,
			'expires' => $expires
		]);

		return $token;
	}


	/**
	 * Find token.
	 *
	 * @param $value
	 *
	 * @return Token
	 */
	public function find($value)
	{
		return $this->_token->where('value', '=', $value)
							->where('active', '=', 1)
							->where('deleted', '=', 0)
							->first();
	}


	/**
	 * Deactivate token.
	 *
	 * @param Token $token
	 */
	public function deactivate(Token $token)
	{
		$token->expires = null;
		$token->active = 0;
		$token->save();
	}
}