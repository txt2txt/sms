<?php

namespace SMS\Repositories;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use SMS\Eloquent\User;
use SMS\Enums;

/**
 * Handles only user data manipulation.
 *
 * No logic should be placed in this class.
 * This class is for data manipulation only.
 *
 * Class UserRepository
 * @package SMS\Repositories
 */
class UserRepository
{
	/**
	 * Eloquent user class.
	 *
	 * @var User $_user
	 */
	private $_user;


	/**
	 * UserRepository constructor.
	 *
	 * @param User $user
	 */
	public function __construct(User $user)
	{
		$this->_user = $user;
	}


	/**
	 * Find user by id.
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function byId($id)
	{
		return $this->_user->find($id)->first();
	}


	/**
	 * Find user by url id.
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function byUrlId($id)
	{
		return $this->_user->where('url_id', $id)->first();
	}


	/**
	 * Find user by url id and email.
	 *
	 * @param $urlId
	 * @param $email
	 *
	 * @return mixed
	 */
	public function byUrlIdAndEmail($urlId, $email)
	{
		return $this->_user->where('url_id', $urlId)
		                   ->where('email', $email)
		                   ->first();
	}


	/**
	 * Find user by email.
	 *
	 * @param $email
	 *
	 * @return mixed
	 */
	public function byEmail($email)
	{
		return $this->_user->where('email', $email)
		                 ->first();
	}


	/**
	 * Find user by phone number.
	 *
	 * @param $countryId
	 * @param $number
	 *
	 * @return mixed
	 */
	public function byPhone($countryId, $number)
	{
		return $this->_user->where('country_id', $countryId)
		                 ->where('phone_number', $number)
		                 ->first();
	}


	/**
	 * Process successful login.
	 *
	 * @param User $user
	 */
	public function successfullyLoggedIn(User $user)
	{
		$user->last_login = now();
		$user->attempts = 0;
		$user->save();
	}


	/**
	 * Sign user up.
	 *
	 * @param Request $request
	 *
	 * @return User
	 */
	public function signUp(Request $request) : User
	{
		$this->_user->first_name = $request->input('fname');
		$this->_user->last_name = $request->input('lname');
		$this->_user->email = $request->input('email');
		$this->_user->country_id = 232;
		$this->_user->phone_number = '';
		$this->_user->password = '';
		$this->_user->status = Enums\UserStatuses::$Unactivated;
		$this->_user->url_id = uniqueUserIdentifier();
		$this->_user->save();

		// Return user
		return $this->_user;
	}


	/**
	 * Activate user's account.
	 *
	 * @param User $user
	 *
	 * @return string
	 */
	public function activate(User &$user)
	{
		$user->status = Enums\UserStatuses::$Active;
		$user->save();

		return $user->loginUrl();
	}


	/**
	 * Set user's password
	 *
	 * @param User $user
	 * @param $password
	 */
	public function setPassword(User &$user, $password)
	{
		$user->password = Hash::make($password);
		$user->save();
	}


	/**
	 * Regenerate user's id.
	 *
	 * @param User $user
	 *
	 * @return string
	 */
	public function regenerateId(User $user) : string
	{
		$user->url_id = uniqueUserIdentifier();
		$user->status = 1;
		$user->date_locked = null;
		$user->attempts = 0;
		$user->save();

		return $user->loginUrl();
	}


	/**
	 * Process failed login attempts.
	 *
	 * @param User $user
	 */
	public function incrementLoginAttempts(User $user)
	{
		$userAttempts = $user->attempts;
		$user->attempts = $userAttempts + 1;
		$user->save();
	}


	/**
	 * Lock user out.
	 *
	 * @param User $user
	 */
	public function lock(User $user)
	{
		$user->status = Enums\UserStatuses::$Locked;
		$user->date_locked = \Carbon\Carbon::now();
		$user->save();
	}


	/**
	 * Unlock user.
	 *
	 * @param User $user
	 *
	 * @return mixed
	 */
	public function unlock(User $user) : string
	{
		$user->status = Enums\UserStatuses::$Active;
		$user->date_locked = null;
		$user->attempts = 0;
		$user->save();

		return $user->loginUrl();
	}


	/**
	 * Update a user's status.
	 *
	 * @param User $user
	 * @param $status
	 */
	public function updateUserStatus(User &$user, $status)
	{
		$user->status = $status;
		$user->save();
	}


	/**
	 * Create or update a config value.
	 *
	 * @param User $user
	 * @param $name
	 * @param $value
	 */
	public function updateConfig(User $user, $name, $value)
	{
		$oldConfig = $user->config($name, 'nonexistent');

		if($oldConfig == 'nonexistent')
		{
			$user->configs()->create([
				'name' => $name,
				'value' => $value
			]);
		}
		else
		{
			$user->configs()->where('name', '=', $name)
				->update([
					'value' => $value
				]);
		}
	}


	/**
	 * Update user's password set date.
	 *
	 * This method needs to be called prior to saveCurrentPasswordHistory().
	 * The date returned from this method needs to be passed into the saveCurrentPasswordHistory()
	 * method.
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Support\Carbon
	 */
	public function updatePasswordSetDate(User &$user)
	{
		$dateReset = now();
		$this->updateConfig($user, 'password_set', $dateReset);
		return $dateReset;
	}


	/**
	 * Save a user's current password to history.
	 *
	 * This needs to be called prior to the passwordReset() method.
	 *
	 * @param User $user
	 * @param $dateReset
	 */
	public function saveCurrentPasswordHistory(User $user, $dateReset)
	{
		$user->oldPasswords()->create([
			'password' => $user->password,
			'date_reset' => $dateReset
		]);
	}


	/**
	 * Add a user's role.
	 *
	 * @param User $user
	 * @param array $roleIds
	 */
	public function syncRoles(User &$user, array $roleIds)
	{
		$user->roles()->sync($roleIds);
	}


	/**
	 * Find a user's activity.
	 *
	 * @param $userId
	 *
	 * @return mixed
	 */
	public function activity($userId)
	{
		return $this->byId($userId)->activity;
	}
}