<?php

namespace SMS\Repositories;

use SMS\Eloquent\User;
use SMS\Eloquent\LogError;
use SMS\Eloquent\LogUser;
use SMS\Eloquent\LogEmail;
use SMS\Repositories\Contracts\ILoggingRepository;
use Throwable;

class LoggingRepository implements ILoggingRepository
{
	/**
	 *
	 *
	 * @var LogError
	 */
	private $_errorLog;


	/**
	 * User log model
	 *
	 * @var LogUser
	 */
	private $_userLog;


	/**
	 * Email log model
	 *
	 * @var LogEmail
	 */
	private $_emailLog;


	/**
	 * LoggingRepository constructor.
	 *
	 * @param LogError $errorLog
	 * @param LogUser $userLog
	 * @param LogEmail $emailLog
	 */
	public function __construct(LogError $errorLog, LogUser $userLog, LogEmail $emailLog)
	{
		$this->_errorLog = $errorLog;
		$this->_userLog = $userLog;
		$this->_emailLog = $emailLog;
	}


	/**
	 * Log errors
	 *
	 * @param Throwable $throwable
	 *
	 * @return void
	 */
	public function logError(Throwable $throwable)
	{
		$this->_errorLog->exception = $throwable->getMessage();
		$this->_errorLog->inner_exception = $throwable->getTraceAsString();
		$this->_errorLog->file = $throwable->getFile();
		$this->_errorLog->code = $throwable->getCode();
		$this->_errorLog->previous = $throwable->getPrevious();
		$this->_errorLog->save();
	}


	/**
	 * Log user activity.
	 *
	 * @param User $user
	 * @param int $eventType
	 * @param string $notes
	 */
	public function logActivity(User $user, int $eventType, string $notes="")
	{
		$this->_userLog->user_id = $user->user_id;
		$this->_userLog->event_type_id = $eventType;
		$this->_userLog->ip = request()->ip();
		$this->_userLog->notes = $notes;
		$this->_userLog->user_agent = request()->userAgent();
		$this->_userLog->save();
	}


	/**
	 * Log sent email.
	 *
	 * @param $userId
	 * @param string $subject
	 * @param string $to
	 * @param string $from
	 * @param string $body
	 * @param string $fullMessage
	 * @param $dateSent
	 */
	public function logEmail($userId, string $subject, string $to, string $from, string $body, string $fullMessage, $dateSent)
	{
		$this->_emailLog->user_id = $userId;
		$this->_emailLog->subject = $subject;
		$this->_emailLog->to = $to;
		$this->_emailLog->from = $from;
		$this->_emailLog->body = $body;
		$this->_emailLog->date_sent = $dateSent;
		$this->_emailLog->full_message = $fullMessage;
		$this->_emailLog->save();
	}
}