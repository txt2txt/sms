<?php

namespace SMS\Enums;

class SessionNames
{
	// Redirect url session name.
	public static $redirectUrl      = 'redirectUrl';
	// Url ID session name.
	public static $urlId            = 'urlId';
	// User ID session name.
	public static $userId           = 'userId';
	// Password reset session name.
	public static $PasswordReset    = 'passwordReset';
	// Page session name.
	public static $PageName         = 'pageName';
}