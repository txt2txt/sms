<?php

namespace SMS\Enums;

class UserStatuses
{
	public static $Unactivated  = 0;
	public static $Active       = 1;
	public static $Deactivated  = 2;
	public static $Suspended    = 3;
	public static $Locked       = 4;
	public static $ReqPassReset = 5;
}