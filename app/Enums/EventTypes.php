<?php

namespace SMS\Enums;

class EventTypes
{
	public static $UserLoginRequest     = 1;
	public static $UserAuthSuccess      = 2;
	public static $UserAuthFail         = 3;
	public static $UserSignUp           = 4;
	public static $UserAccountActivated = 5;
	public static $UserProfileModified  = 6;
	public static $UserPasswordUpdate   = 7;
	public static $UserRegenerateUrlId  = 8;
	public static $UserAccountDeleted   = 9;
	public static $UserLogout           = 10;
	public static $UserResetPasswordReq = 11;
	public static $UserResetPassword    = 12;
	public static $UserAccountLocked    = 13;
	public static $UserAccountUnlocked  = 14;
}