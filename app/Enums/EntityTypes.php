<?php

namespace SMS\Enums;

class EntityTypes
{
	public static $UserTypeId = 1;
	public static $RecipientTypeId = 2;
	public static $RecipientGroupTypeId = 3;
	public static $MessageQueuedTypeId = 4;
	public static $MessageSentTypeId = 5;
	public static $MessageReceivedTypeId = 6;
}