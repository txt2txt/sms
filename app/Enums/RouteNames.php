<?php

namespace SMS\Enums;

class RouteNames
{
	public static $Logout                       = 'logout';
	public static $ProfileHome                  = 'profile-home';
	public static $ProfileContactInfo           = 'profile-contact-info';
	public static $ProfilePersonalInfo          = 'profile-personal-info';
	public static $ProfilePreferences           = 'profile-preferences';
	public static $ProfileActivity              = 'profile-activity';

	public static $ProfilePasswordReset         = 'profile-password-reset';
	public static $ProfilePasswordResetProcess  = 'profile-password-reset-process';
}