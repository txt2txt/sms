<?php

namespace SMS\Enums;

class TokenTypes
{
	public static $AccountActivation    = 1;
	public static $AccountUnlock        = 2;
	public static $AccountRegenerateId  = 3;
	public static $PasswordReset        = 4;
}