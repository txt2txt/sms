<?php

namespace SMS\Enums;

class Settings
{
	public static $FailedLoginSessionId             = 'failed-login-count';
	public static $FailedLoginAttemptsRemaining     = 'failed-login-count';
	public static $FailedLoginMaxAttempts           = 5;
	public static $AccountTokenExpiresInMinutes     = 30;
	public static $PasswordResetTimeInDays          = 90;
	public static $LoginRouteName                   = 'login';
	public static $LoginProcessRouteName            = 'login-process';
}