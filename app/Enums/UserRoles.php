<?php

namespace SMS\Enums;

class UserRoles
{
	// Site roles.
	public static $SuperAdmin   = 1;
	public static $CustomerService = 2;
	// Customer roles.
	public static $Admin        = 3;
	public static $Manager      = 4;
	public static $Editor       = 5;
	public static $Author       = 6;
	public static $Recipient    = 7;
}