<?php

return [

	// Route names allows to pass the UserRestriction middleware
	'allowed-routes-names' => [
		'account-activate',
		'password-forgot',
		'password-reset-by-email',
		'password-reset-by-email-process',
		'password-reset-by-questions',
		'password-reset-by-questions-process',
		'password-reset-email-token',
		'password-reset-email-token-process',
		'unlock-account',
		'regenerate-url'
	],

	'url-allowed-keywords' => [
		'regenerate',
		'reset',
		'unlock',
		'activate',
		'forgot',
	],

	'questions' => [
		'What Is your favorite book?',
		'What is the name of the road you grew up on?',
		'What is your mother’s maiden name?',
		'What was the name of your favorite pet?',
		'What was the name of your first car?',
		'What was the first company that you worked for?',
		'Where did you meet your spouse?',
		'What was your favorite school/college/university you attended?',
		'What is your favorite food?',
		'Where is your favorite place to vacation?'
	]
];