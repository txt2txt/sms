<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use SMS\Enums\RouteNames;

// Routes for user's account
Route::group(['middleware'=> ['userVerification'],'prefix'=>'u/{userId}/account'], function ()
{
	// Login routes
	Route::get('/login', 'Account\LoginController@loginShow')
	     ->name('login');
	Route::post('/login', 'Account\LoginController@loginProcess')
	     ->name('login-process');

	// Logout route
	Route::get('/logout', 'Account\LoginController@logout')
	     ->name('logout');

	// Activate ner user.
	Route::get('/activate/{token}', 'Account\AccountController@activate')
	     ->name('account-activate');

	// Show forgot password page.
	Route::get('/password-forgot', 'Account\PasswordController@passwordForgot')
	     ->name('password-forgot');

	// Show email form for password reset and then process posted form.
	Route::get('/reset-password/email', 'Account\PasswordController@resetPasswordByEmail')
	     ->name('password-reset-by-email');
	Route::post('/reset-password/email', 'Account\PasswordController@resetPasswordByEmailProcess')
	     ->name('password-reset-by-email-process');

	// Show security question form and then process posted form.
	Route::get('/reset-password/security-questions', 'Dashboard\SecurityController@resetPasswordSecurityQuestions')
	     ->name('password-reset-by-questions');
	Route::post('/reset-password/security-questions', 'Dashboard\SecurityController@resetPasswordSecurityQuestionsProcess')
	     ->name('password-reset-by-questions-process');

	// Process password reset by emailed token.
	Route::get('/reset-password/{token}', 'Account\PasswordController@resetPassword')
	     ->name('password-reset-email-token');
	Route::post('/reset-password/{token}', 'Account\PasswordController@resetPasswordProcess')
	     ->name('password-reset-email-token-process');

	// Unlock user
	Route::get('/unlock/{token}', 'Account\AccountController@unlock')
	     ->name('unlock-account');

	// Regenerate user's url
	Route::get('/regenerate-url/{token}', 'Account\AccountController@regenerateId')
	     ->name('regenerate-url');
});


// Routes for user's profile
Route::group(['middleware'=>['userVerification'],'prefix'=>'u/{userId}/account/profile'], function ()
{
	// Profile home.
	Route::get('/', 'Account\ProfileController@homeShow')
	     ->name(RouteNames::$ProfileHome);

	// Profile activity.
	Route::get('/activity', 'Account\ProfileController@activityShow')
	     ->name(RouteNames::$ProfileActivity);

	// Profile reset password.
	Route::get('/reset-password', 'Account\PasswordController@resetPasswordFromProfileShow')
	     ->name(RouteNames::$ProfilePasswordReset);
	Route::post('/reset-password', 'Account\PasswordController@resetPasswordFromProfileShow')
	     ->name(RouteNames::$ProfilePasswordResetProcess);
});

// Routes for user's dashboard
Route::group(['middleware'=>['userVerification', 'userAuth'],'prefix'=>'u/{userId}/dash'], function ()
{
	Route::get('/', 'Dashboard\HomeController@index')->name('dash-home');

});



// Generic routes.
Route::get('/', 'HomeController@index')->name('home');
Route::get('/features', 'HomeController@index')->name('features');
Route::get('/pricing', 'HomeController@index')->name('pricing');
Route::get('/about-us', 'HomeController@about')->name('about');
Route::get('/connect', 'ContactController@get')->name('connect');
Route::get('/connect', 'ContactController@post')->name('connect-post');
Route::get('/request-login', 'Account\LoginController@requestLoginShow')->name('request-login');
Route::post('/request-login', 'Account\LoginController@requestLoginProcess')->name('request-login-post');
Route::get('/sign-up', 'RegisterController@registerShow')->name('sign-up');
Route::post('/sign-up', 'RegisterController@registerProcess')->name('sign-up-post');